FROM node:13.12.0-alpine as build
RUN mkdir /frontend
WORKDIR /frontend
ENV PATH /frontend/node_modules/.bin:$PATH

COPY package.json /frontend
COPY package-lock.json /frontend
RUN npx browserslist@latest --update-db
RUN npm install && npm fund

COPY . /frontend/
RUN npm run build:dev

# production environment
FROM nginx:stable-alpine
COPY --from=build /frontend/dist /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8888
CMD ["nginx", "-g", "daemon off;"]
