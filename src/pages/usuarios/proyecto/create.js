import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik, yupToFormErrors } from 'formik';
import * as Yup from 'yup';
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';
import { AuthContext } from '../../../context/AuthContext';


const UsrCreatex = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const [rols, setRols] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  const request = async (idProyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/roles/${idProyecto}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    return data;
  };

  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  useEffect(() => {
    setIsLoading(true);
    request(proyecto.codigo).then((data) => {
      setRols(data);console.log(data)
      setIsLoading(false);
    });
  }, []);
  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Usuarios</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        {!isLoading?
        <Formik
          initialValues={{
            username_email: '',
            password: '',
            llavepassword: '',
            nombre: '',
            apellido: '',
            activo: '',
            rol:'',
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/user/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            username_email: Yup.string().email().required('Campo Obligatorio').trim(),
            password: Yup.string()
              .required('No se coloco contraseña')
              .min(6, 'Contraseña muy corta'),
            llavepassword: Yup.string().nullable().trim(),
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            apellido: Yup.string().required('Campo Obligatorio').trim(),
            activo: Yup.string().required('Campo Obligatorio').trim(),
            rol: Yup.string().required('Campo Obligatorio'),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Email del Usuario
                </label>
                <input
                  id="username_email"
                  type="Text"
                  title="Ingrese el email del Usuario"
                  placeholder="Ingrese el email del Usuario"
                  value={values.username_email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.username_email && touched.username_email
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.username_email && touched.username_email && (
                  <div className="input-feedback">{errors.username_email}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Contraseña
                </label>
                <input
                  id="password"
                  placeholder="Ingrese su contraseña"
                  title="En este campo va la contraseña del usuario"
                  type="Text"
                  maxLength="15"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del Usuario
                </label>
                <input
                  id="nombre"
                  placeholder="Ingrese su Nombre"
                  title="Nombre(s) del usuario"
                  type="Text"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Apellido del usuario
                </label>
                <input
                  id="apellido"
                  placeholder="Ingrese su(s) apellido(s)"
                  title="Apellido(s) del usuario"
                  type="Text"
                  maxLength="50"
                  value={values.apellido}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.apellido && touched.apellido
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.apellido && touched.apellido && (
                  <div className="input-feedback">{errors.apellido}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Activo
                </label>
                <select
                  id="activo"
                  placeholder="Activo"
                  title="El usuario se encuentra Activo?"
                  value={values.activo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.activo && touched.activo
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  <option value="N" label="No Activo" />
                  <option value="S" label="Activo" />
                </select>
                {errors.activo && touched.activo && (
                  <div className="input-feedback">{errors.activo}</div>
                )}


                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Rol del Usuario
                </label>
                <select
                  id="rol"
                  placeholder="Rol"
                  title="Rol del Usuario"
                  value={values.rol}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.rol && touched.rol
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  <option disabled value="" label="Seleccionar Rol" />
                  {rols? 
                    rols.map((rol,key)=>
                    {var value=rol.id; return(
                    <option key={key} value={value}>{rol.descripcion}</option>
                      )}
                    ):[]
                  }
                  
                </select>
                {errors.rol && touched.rol && (
                  <div className="input-feedback">{errors.rol}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Usuario
                </button>
              </form>
            );
          }}
        </Formik>
        :[]}
      </div>
    </div>
  );
};

export default UsrCreatex;
