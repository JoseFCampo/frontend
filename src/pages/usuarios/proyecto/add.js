import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Spinner } from 'react-bootstrap';

import Message from '../../../components/Message';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';


const PassRst = () => {
    const [roles, setRoles] = useState(null);
    const [users, setUsers] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const { authAxios } = useContext(FetchContext);
    const [resp, setResp] = useState(null);
    const history = useHistory();
    const id = useLocation();
    const { project, isAdmin } = useContext(AuthContext);
    let proyecto = 0;
    if (project) {
        proyecto = JSON.parse(project);
    }
    const handleBack = () => {
        history.goBack();
    };
    const request = async (idProyecto) => {
        const response = await authAxios.get(
            `${process.env.REACT_APP_API_URL}/user/roles/${idProyecto}/`,
            {
                crossdomain: true,
            },
        );
        const data = await response.data;
        return data;
    };
    const request1 = async (idProyecto) => {
        const response = await authAxios.get(
            `${process.env.REACT_APP_API_URL}/user/add/${idProyecto}/`,
            {
                crossdomain: true,
            },
        );
        const data = await response.data;
        return data;
    };

    useEffect(() => {
        setIsLoading(true);
        request(proyecto.codigo).then((data) => {
            setRoles(data);
        });
        request1(proyecto.codigo).then((data) => {
            console.log(data)
            setUsers(data);
            setIsLoading(false);
        });

    }, []);

    return (
        <div>
            <div>
                <div className="Add">
                    <h1>Formulario de Vinculacion de Usuarios al proyecto:</h1>
                    <h3>
                        {proyecto.titulo} ({proyecto.nombre_alterno})
                    </h3>
                    <br />
                    <div>
                        <button
                            onClick={handleBack}
                            style={{
                                display: 'block',
                            }}
                        >
                            Regresar{' '}
                        </button>
                        {resp ? <Message response={resp} /> : ''}
                    </div>
                    <br />
                </div>
                {!isLoading?
                <Formik
                    initialValues={{
                        id_rol: '',
                        id_usuario: '',
                    }}
                    onSubmit={async (values) => {
                        const respuesta = await authAxios({
                            method: 'post',
                            url: `${process.env.REACT_APP_API_URL}/user/add/`,
                            data: values,
                        });
                        setResp(respuesta);
                    }}
                    validationSchema={Yup.object().shape({
                        id_rol: Yup.number().positive('No Ha Seleccionado Usuario').required('Campo Obligatorio'),
                        id_usuario: Yup.number().positive('No Ha Seleccionado Usuario').required('Campo Obligatorio'),

                    })}
                >
                    {(props) => {
                        const {
                            values,
                            touched,
                            errors,
                            dirty,
                            isSubmitting,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            handleReset,
                        } = props;
                        return (
                            <form onSubmit={handleSubmit}>
                                <label
                                    style={{
                                        display: 'block',
                                    }}
                                >
                                    Rol del Usuario
                </label>
                                <select
                                    id="id_rol"
                                    placeholder="Rol del Usuario"
                                    title="Rol del Usuario"
                                    value={values.id_rol}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                        errors.id_rol && touched.id_rol
                                            ? 'text-input error'
                                            : 'text-input'
                                    }
                                >
                                    <option key='-1' value='-1'>Seleccionar Rol</option>
                                    {roles ?
                                        roles.map((rol, key) => (
                                            <option key={key} value={rol.id}>{rol.descripcion}</option>
                                        )
                                        ) : []
                                    }

                                </select>
                                {errors.id_rol && touched.id_rol && (
                                    <div className="input-feedback">{errors.id_rol}</div>
                                )}


                                <label
                                    style={{
                                        display: 'block',
                                    }}
                                >
                                    Usuario
                                </label>
                                <select
                                    id="id_usuario"
                                    placeholder="Rol del Usuario"
                                    title="Rol del Usuario"
                                    value={values.id_usuario}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                        errors.id_usuario && touched.id_usuario
                                            ? 'text-input error'
                                            : 'text-input'
                                    }
                                >
                                    <option key='-1' value='-1'>Seleccionar Usuario</option>
                                    {users ?
                                        users.map((user, key) => (
                                            <option key={key} value={user.id_usuario}>{user.nombre} {user.apellido} ({user.username_email})</option>
                                        )
                                        ) : []
                                    }

                                </select>
                                {errors.id_usuario && touched.id_usuario && (
                                    <div className="input-feedback">{errors.id_usuario}</div>
                                )}
                                <br />
                                <button
                                    type="button"
                                    className="outline"
                                    onClick={handleReset}
                                    disabled={!dirty || isSubmitting}
                                >
                                    Limpiar Formulario
                </button>
                                <button type="submit" disabled={isSubmitting}>
                                    Agregar Usuario al Proyecto
                </button>
                            </form>
                        );
                    }}
                </Formik>:[]}
            </div>
        </div>
    );
};
export default PassRst;
