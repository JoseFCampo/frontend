import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';

const errorMsg = 'Please enter a password';

const PassRst = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const id = useLocation();
  const idUsuario = id.search.split('=')[1];
  // console.log(idUsuario);

  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Cambio de Contraseña</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            password: '',
            passwordConfirmation: '',
          }}
          onSubmit={async (values) => {
            const data = {
              id_usuario: idUsuario,
              password: values.password,
            };

            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/user/passwordReset/`,
              data,
            });
            setResp(respuesta);
          }}
          validationSchema={Yup.object().shape({
            password: Yup.string().required(errorMsg),
            passwordConfirmation: Yup.string()
              .oneOf([Yup.ref('password'), null], 'Las contraseñas no coinciden')
              .required('Confirm Password is required'),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Contraseña
                </label>
                <input
                  id="password"
                  type="password"
                  title="Ingrese la Contraseña(min 6 Caracteres)"
                  placeholder="Ingrese la Nueva Contraseña(min 6 Caracteres)"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}{' '}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Confirmar Contraseña
                </label>
                <input
                  id="passwordConfirmation"
                  type="password"
                  title="Ingrese la Contraseña(min 6 Caracteres)"
                  placeholder="Ingrese la contraseña"
                  value={values.passwordConfirmation}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.passwordConfirmation && touched.passwordConfirmation
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.passwordConfirmation && touched.passwordConfirmation && (
                  <div className="input-feedback">{errors.passwordConfirmation}</div>
                )}{' '}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Contraseña
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default PassRst;
