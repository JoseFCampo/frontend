import React, { useState, useEffect, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../../components/Table';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const UserView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (idProyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/a/${idProyecto}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_usuario}`;
      row.psswd = `${row.id_usuario}`;
      row.det = `${row.id_usuario}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [usrs, setUsrs] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  useEffect(() => {
    setIsLoading(true);
    request(proyecto.codigo).then((data) => {
      setUsrs(data);
      setIsLoading(false);
    });
  }, []);

  if (usrs === null) {
    return (
      <div>
        <h1>Usuarios del Proyecto</h1>

        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }
  const handleCrear = () => {
    history.push('/usrCreate');
  };
  const handleAgregar = () => (
    history.push('/usrAdd')
  );
  const columns = [
    {
      Header: 'Usuario',
      columns: [
        {
          Header: 'Rol',
          accessor: 'descripcion',
          width: 100,
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Id del Usuario',
          accessor: 'id_usuario',
        },

        {
          Header: 'Nombre',
          accessor: 'nombre',
        },
        {
          Header: 'Apellido',
          accessor: 'apellido',
        },
        {
          Header: 'Email',
          accessor: 'username_email',
        },
        {
          Header: 'Activo',
          accessor: 'activo',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/usrUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
        {
          Header: 'Contraseña',
          width: 70,
          disableFilters: true,
          accessor: 'psswd',
          Cell: (row) => (
            <div>
              <Link to={`/usrPasswordReset?id=${row.value}`}> 🛑</Link>
            </div>
          ),
        },
        {
          Header: 'Detalles',
          width: 70,
          disableFilters: true,
          accessor: 'det',
          Cell: (row) => (
            <div>
              <Link to={`/usrDetails?id=${row.value}`}> 🔎</Link>
            </div>
          ),
        },
      ],
    });
  }
  // console.log(proyecto);
  return (
    <div>
      <h1>Usuarios del Proyecto</h1>
      <h3>
        {proyecto.titulo} ({proyecto.nombre_alterno})
      </h3>
      {isAdmin() ? (<>
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
        <button
        onClick={() => handleAgregar()}
        style={{
          float: 'rigth',
          display: 'inline-block',
          marginLeft: '2px',
        }}
      >
        Agregar
      </button>
      </>
      ) : (
        []
      )}
      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={usrs && usrs ? usrs : []} columns={columns} />
      )}
    </div>
  );
};

export default UserView;
