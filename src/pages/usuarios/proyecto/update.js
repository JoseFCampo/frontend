import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';
import { AuthContext } from '../../../context/AuthContext';

const UsrUpdateV = () => {
  const { authAxios } = useContext(FetchContext);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }

  const request = async (idUsuario,idProyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/u/${idUsuario}/${idProyecto}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const request1 = async (idProyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/roles/${idProyecto}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    return data;
  };  

  const [rols, setRols] = useState(null);
  const [usr, setUsr] = useState(null);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const id = useLocation();
  const idUsuario = id.search.split('=')[1];

  useEffect(() => {
    request(idUsuario,proyecto.codigo).then((data) => {
      setUsr(data.data);
    });
    request1(proyecto.codigo).then((data) => {
      setRols(data);
    });
  }, []);

  const handleBack = () => {
    history.goBack();
  };
  if (usr === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Usuarios...</p>
      </div>
    );
  }

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario actualización de Usuarios</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            id: idUsuario,
            username_email: usr.username_email,
            llavepassword: usr.llavepassword?usr.llavepassword:'',
            nombre: usr.nombre,
            apellido: usr.apellido,
            activo: usr.activo,
            rol: usr.id_rol,
            rol_a:usr.id_rol
          }}

          onSubmit={async (values) => {
            console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/user/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            llavepassword: Yup.string().nullable().trim(),
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            apellido: Yup.string().required('Campo Obligatorio').trim(),
            activo: Yup.string().required('Campo Obligatorio').trim(),
            rol: Yup.number().required('Campo Obligatorio'),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Id del Usuario
                </label>
                <input
                  id="id"
                  type="Text"
                  title="Ingrese el email del Usuario"
                  placeholder="Ingrese el email del Usuario"
                  value={values.id}
                  disabled
                  className={
                    errors.id && touched.id ? 'text-input error' : 'text-input'
                  }
                />
                {errors.id && touched.id && (
                  <div className="input-feedback">{errors.id}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Email del Usuario
                </label>
                <input
                  id="username_email"
                  type="Text"
                  title="Ingrese el email del Usuario"
                  placeholder="Ingrese el email del Usuario"
                  value={values.username_email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.username_email && touched.username_email
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.username_email && touched.username_email && (
                  <div className="input-feedback">{errors.username_email}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del Usuario
                </label>
                <input
                  id="nombre"
                  placeholder="Ingrese su Nombre"
                  title="Nombre(s) del usuario"
                  type="Text"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Apellido del usuario
                </label>
                <input
                  id="apellido"
                  placeholder="Ingrese su(s) apellido(s)"
                  title="Apellido(s) del usuario"
                  type="Text"
                  maxLength="50"
                  value={values.apellido}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.apellido && touched.apellido
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.apellido && touched.apellido && (
                  <div className="input-feedback">{errors.apellido}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Activo
                </label>
                <select
                  id="activo"
                  placeholder="Activo"
                  title="El usuario se encuentra Activo?"
                  value={values.activo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.activo && touched.activo
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  <option value="N" label="No Activo" />
                  <option value="S" label="Activo" />
                </select>
                {errors.activo && touched.activo && (
                  <div className="input-feedback">{errors.activo}</div>
                )}


                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Rol del Usuario
                </label>
                <select
                  id="rol"
                  placeholder="Rol del Usuario"
                  title="Rol del Usuario"
                  value={values.rol}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.rol && touched.rol
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  {rols? 
                    rols.map((rol,key)=>(
                    <option key={key} value={rol.id}>{rol.descripcion}</option>
                      )
                    ):[]
                  }
                  
                </select>
                {errors.rol && touched.rol && (
                  <div className="input-feedback">{errors.rol}</div>
                )}



                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Usuario
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default UsrUpdateV;
