import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../../components/Message';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const RolUpdateV = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (idRol) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/roles/u/${idRol}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };


  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  const [rol, setRol] = useState(null);
  const [pers, setPers] = useState(null);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const id = useLocation();
  const idRol = id.search.split('=')[1];

  useEffect(() => {
    request(idRol).then((data) => {
      // console.log(data)
      setRol(data.data);
    });

  }, []);

  const handleBack = () => {
    history.goBack();
  };
  if (rol === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Roles...</p>
      </div>
    );
  }

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Actualización de Roles</h1>
          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            id: rol.id,
            descripcion: rol.descripcion,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/user/roles/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            descripcion: Yup.string().required('Campo Obligatorio').trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador del Rol
                </label>
                <input
                  id="id"
                  type="Text"
                  title="Id del Rol"
                  placeholder="Id del Rol"
                  value={values.id}
                  disabled
                  className={
                    errors.id && touched.id ? 'text-input error' : 'text-input'
                  }
                />
                {errors.id && touched.id && (
                  <div className="input-feedback">{errors.id}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Rol
                </label>
                <input
                  id="descripcion"
                  type="Text"
                  title="Ingrese la Descripción del Rol"
                  placeholder="Ingrese la Descripción del Rol"
                  value={values.descripción}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                 Actualizar Rol
                </button>
              </form>
            );
          }}
        </Formik>

      </div>
    </div>
  );
};

export default RolUpdateV;
