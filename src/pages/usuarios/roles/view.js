import React, { useState, useEffect, useContext } from 'react';

import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../../components/Table';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const RolesView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (idProyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/roles/${idProyecto}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id}`;
      row.permisos = `${row.id}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [roles, setRoles] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  useEffect(() => {
    setIsLoading(true);
    request(proyecto.codigo).then((data) => {
      setRoles(data);
      setIsLoading(false);
    });
  }, []);

  if (roles === null) {
    return (
      <div>
        <h1>Roles del Proyecto</h1>

        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }
  const handleCrear = () => {
    history.push('/RolCreate');
  };

  const columns = [
    {
      Header: 'Rol',
      columns: [
        {
          Header: 'Id del rol',
          accessor: 'id',
        },
        {
          Header: 'Descripción',
          accessor: 'descripcion',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/rolUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
        {
          Header: 'Permisos',
          width: 70,
          disableFilters: true,
          accessor: 'permisos',
          Cell: (row) => (
            <div>
              <Link to={`/PermisosRol?id=${row.value}`}>🔎</Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Roles del Proyecto</h1>
      <h3>
        {proyecto.titulo} ({proyecto.nombre_alterno})
      </h3>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
          []
        )}

      {isLoading ? (
        <p>Cargando pagina</p>
      ) : (
          <Table data={roles && roles ? roles : []} columns={columns} />
        )}
    </div>
  );
};

export default RolesView;
