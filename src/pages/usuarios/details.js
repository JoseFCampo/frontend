import React, { useState, useEffect, useContext } from 'react';
 
import '../../static/styles/forms.scss';
import '../../static/styles/proyectos.scss';
import { Spinner } from 'react-bootstrap';
import { useLocation, useHistory } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const DetView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (idUsuario) => {
    const proy = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/du/${idUsuario}/`,
      {
        crossdomain: true,
      },
    );
    const resps = await proy.data;
    return resps;
  };

  const [det, setDet] = useState(null);
  const { authState } = useContext(AuthContext);
  const idUsuario = authState.userInfo.id;
  // console.log(idUsuario);

  useEffect(() => {
    request(idUsuario).then((data) => {
      console.log(data);
      setDet(data);
    });
  }, []);

  return (
    <>
      {det ? (
        <div className="wrap-fluid">
          <div className="container-fluid paper-wrap bevel tlbr">
            <div className="content-wrap">
              <div className="col-sm-12">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="nest">
                      <div className="title-alt">
                        <h3
                          style={{
                            padding: '12px',
                          }}
                        >
                          Usuario
                        </h3>
                      </div>
                      <div className="body-nest">
                        <h2>
                          {det[0].id_usuario.nombre} {det[0].id_usuario.apellido}
                        </h2>
                        <br />
                        <dl className="dl-horizontal-profile" />
                        <br />
                      </div>
                      <div className="body-nest">
                        <dl className="dl-horizontal-profile">
                          {det.map((value) =>
                            value.codigo_atributo ? (
                              <>
                                <dt>{value.codigo_atributo.descripcion}</dt>
                                <dd>{value.valor_asignado}</dd>
                                <br />
                              </>
                            ) : (
                              []
                            ),
                          )}
                        </dl>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        []
      )}
    </>
  );
};

export default DetView;
