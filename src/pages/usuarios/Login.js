import '../../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../../static/styles/login.scss';
import { Formik } from 'formik';
import * as Yup from 'yup';
import React, { useContext,useState} from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import Axios from 'axios'
import Logo from '../../components/LogoInvemar';

const Login = () => {
  const { authState, setAuthState, logout } = useContext(AuthContext);
  const history = useHistory();
  const [messLogin,setMessLogin]=useState("")
  return (
    <div
      className="container"
      style={{
        margin: 'auto',
      }}
    >
      <div className="" id="login-wrapper">
        <div
          className="row"
          style={{
            margin: '0px',
          }}
        >
          <div className="col-md-8">
            <div className="row">
              <div className="col-md-12">
                <div id="logo-login1">
                <div className="row" style={{justifyContent:"center"}}>
                    <Logo
                      href="#"
                      className="profile-img"
                      alt="INVEMAR"
                      height="80px"
                    />
                </div>
                    <h1 style={{fontWeight:"500"}}>
                    ARGOS - Sistema de Soporte Multitemático para el Monitoreo
                    Ambiental  
                    <span>  v2.0</span>
                  </h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="account-box" >
                  <p>
                  Módulo para usuarios regsitrados que permite:
                  </p>
                  <p>
                    - Consultar y descargar de acuerdo a su perfil de usuario, datos por proyecto o actividad de monitoreo.
                  </p>
                  <sppan>
                    - A los usuarios administradores, registrar otros usuarios, actualizar listas controladas para variables, especies y puntos de monitoreo.
                  </sppan>
                  <br/>
                  <p>
                  Preguntas y soporte técnico por favor escribir a 
                  <a href="mailto:julian.pizarro@invemar.org.co"> julian.pizarro@invemar.org.co</a>
                  </p>
                  {/* <a className="forgotLnk" href="index.html" />
                  <div className="or-box">
                    <center>
                      <span className="text-center login-with">
                        Ingrese al proyecto <b>Demo</b>
                      </span>
                    </center>
                    <div className="row">
                      <div className="col-md-6 row-block">
                        <a href="index.html" className="btn btn-instagram btn-block">
                          <span className="entypo-facebook space-icon" />
                          Proyecto demo
                        </a>
                      </div>
                    </div>
                  </div>
                  <hr>
                    <div className="row-block">
                      <div className="row">
                        <div className="col-md-12 row-block">
                          <div style="text-align:center;margin:0 auto;">
                            <h6>Release Candidate 1.0 Powered by © Invemar 2014</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </hr> */}
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="row">
              <div className="col-md-12">
                <div id="logo-login">
                  <div className="row" style={{justifyContent:"center"}}>
                    <Logo
                      href="#"
                      className="profile-img"
                      alt="INVEMAR"
                      height="75px"
                    />
                  </div>
                  <h1>
                    INICIO DE SESIÓN
                    <span>v2.0</span>
                  </h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="account-box">
                  <Formik
                    initialValues={{
                      username: '',
                      password: '',
                    }}
                    onSubmit={async (values) => {
                      try{
                        const respuesta = await Axios({
                          method: 'post',
                          url: `${process.env.REACT_APP_API_URL}/user/login/`,
                          data: values,
                        });

                        const { data } = respuesta;
                        setAuthState(data);
                        console.log(respuesta);
                        history.push('/prj');}
                      catch(error){
                        setMessLogin(error.response)
                        console.log(error.response)
                        }

                      }
                    }
                    validationSchema={Yup.object().shape({
                      password: Yup.string().required('No se ha introducido una contraseña'),
                    })}
                  >
                    {(props) => {
                      const {
                        values,
                        touched,
                        errors,
                        dirty,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        handleReset,
                      } = props;

                      return (
                        <form onSubmit={handleSubmit}>
                          <div className="form-group">
                            <label>Email</label>
                            <input
                              id="username"
                              type="Text"
                              value={values.username}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className={
                                errors.username && touched.username
                                  ? 'text-input error'
                                  : 'text-input'
                              }
                            />
                            {errors.username && touched.username && (
                              <div className="input-feedback">{errors.username}</div>
                            )}
                            {messLogin?messLogin.status===404? <div className="input-feedback">{messLogin.data.message}</div>:[]:[]}

                          </div>

                          <div className="form-group">
                            <label>Contraseña</label>
                            <input
                              id="password"
                              type="password"
                              value={values.password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className={
                                errors.password && touched.password
                                  ? 'text-input error'
                                  : 'text-input'
                              }
                            />
                            {errors.password && touched.password && (
                              <div className="input-feedback">{errors.password}</div>
                            )}

                            {messLogin?messLogin.status===409? 
                              <div className="input-feedback"> 
                                {messLogin.data.message}
                              </div>:[]
                              :[]
                            }
                          </div>
                          
                          <br/>
                       
                        
                          <button
                            type="submit"
                            className="btn btn-primary btn-block"
                          >
                            Iniciar Sesion
                          </button>
                        </form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
