import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory,useLocation } from 'react-router-dom';


import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';
import { AuthContext } from '../../../context/AuthContext';

const AddPerRol = () => {
  const id = useLocation();
  const id_rol = id.search.split('=')[1];
  const { authAxios } = useContext(FetchContext);
  const [isLoading, setIsLoading] = useState(false);
  const [pers, setPers] = useState(null);
  const [rol, setRol] = useState(null);
  const [persel, setPersel] = useState('' );
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const request = async (id_proyecto,id_rol) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/permisos/${id_proyecto}/${id_rol}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    return data;
  };
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  useEffect(() => {
    setIsLoading(true);
    request(proyecto.codigo,id_rol).then((data) => {
      // console.log(data)
      setRol(data.rol);
      setPers(data.data);
      setIsLoading(false);
    });
  }, []);

  const handleChange = (event) => {
    setPersel(event.target.value);
  };

 const handleAdd = async () => {
   if (persel !=0){
     var data={}
     data["id_permiso"]=persel
     data["id_rol"]=id_rol
    // console.log(data);
    const respuesta = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/user/permisosrol/add/`,
      data
    });
    setResp(respuesta);
    // console.log(respuesta);
  }
  }

  return (
    <div>
      <div>
        <div className="add">
          <h1>Formulario Agregacion de Permisos</h1>
          <h3>{rol}</h3>
          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
          <label
            style={{
              display: 'block',
            }}
          >
            Permisos
          </label>
          <select
            id="permisos"
            placeholder="Permisos"
            title="Seleccione el permiso a agregar"
            value={persel}
            onChange={handleChange}
            className={'text-input' }
          >
        <option key="todas" value="0">
          Seleccione el permiso a agregar
        </option>
          {pers ?
            pers.map((value)=>(
                  <option key={value.codigo} value={value.codigo} label={value.descripcion}/>)
              ):[]
          }
          </select>
          
          <button
            type="button"
            className="outline"
            onClick={handleAdd}
            disabled={persel==null}
          >
              Asignar Permiso Al Rol
          </button>
      </div>
    </div>
  );
};

export default AddPerRol;
