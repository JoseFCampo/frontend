import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory,useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';

const PerUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const [per, setPer] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const handleBack = () => {
    history.goBack();
  };

  const request = async (idPermiso) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/permisos/u/${idPermiso}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };
  
  const history = useHistory();
  const id = useLocation();
  const idPermiso = id.search.split('=')[1];


  useEffect(() => {
    request(idPermiso).then((data) => {
      // console.log(data)
      setPer(data.data);
    });
    }, [])

    if (per === null) {
        return (
          <div>
            <p>Cargando formulario de actualización de Roles...</p>
          </div>
        );
      }

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Modificación de Permisos</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            grupo:per.grupo,
            codigo:per.codigo,
            descripcion:per.descripcion,
            descripcion_grupo:per.descripcion_grupo,
            descripcion_completa_cdg:per.descripcion_completa_cdg,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/user/permisos/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            descripcion: Yup.string().required('Campo Obligatorio').trim(),
            grupo:Yup.number().required('Campo Obligatorio'),
            codigo:Yup.number().required('Campo Obligatorio'),
            descripcion:Yup.string().required('Campo Obligatorio').trim(),
            descripcion_grupo:Yup.string().required('Campo Obligatorio').trim(),
            descripcion_completa_cdg:Yup.string().required('Campo Obligatorio').trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Permiso
                </label>
                <input
                  id="descripcion"
                  type="Text"
                  title="Ingrese la descripción del Permiso"
                  placeholder="Ingrese la descripción del Permiso"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}
                

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Código del Permiso
                </label>
                <input
                  id="codigo"
                  type="Text"
                  title="Ingrese la código del Permiso"
                  placeholder="Ingrese la código del Permiso"
                  value={values.codigo}
                  disabled={true}
                  onBlur={handleBlur}
                  className={
                    errors.codigo && touched.codigo
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.codigo && touched.codigo && (
                  <div className="input-feedback">{errors.codigo}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Código
                </label>
                <input
                  id="descripcion_completa_cdg"
                  type="Text"
                  title="Ingrese la descripción_completa_cdg del Permiso"
                  placeholder="Ingrese la descripción_completa_cdg del Permiso"
                  value={values.descripcion_completa_cdg}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion_completa_cdg && touched.descripcion_completa_cdg
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion_completa_cdg && touched.descripcion_completa_cdg && (
                  <div className="input-feedback">{errors.descripcion_completa_cdg}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Grupo del Permiso
                </label>
                <input
                  id="grupo"
                  type="Number"
                  title="Ingrese la grupo del Permiso"
                  placeholder="Ingrese la grupo del Permiso"
                  value={values.grupo}
                  disabled={true}
                  onBlur={handleBlur}
                  className={
                    errors.grupo && touched.grupo
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.grupo && touched.grupo && (
                  <div className="input-feedback">{errors.grupo}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Grupo
                </label>
                <input
                  id="descripcion_grupo"
                  type="Text"
                  title="Ingrese la descripción_grupo del Permiso"
                  placeholder="Ingrese la descripción_grupo del Permiso"
                  value={values.descripcion_grupo}
                  disabled={true}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion_grupo && touched.descripcion_grupo
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion_grupo && touched.descripcion_grupo && (
                  <div className="input-feedback">{errors.descripcion_grupo}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Permiso
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default PerUpdate;
