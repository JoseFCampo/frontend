import React, { useState, useEffect, useContext } from 'react';
import '../../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';
import { AuthContext } from '../../../context/AuthContext';

const PerCreatex = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Permisos</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            descripcion:'',
          }}
          onSubmit={async (values) => {
            values.id_proyecto=proyecto.codigo
            // console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/user/permisos/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            descripcion: Yup.string().required('Campo Obligatorio').trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Permiso
                </label>
                <input
                  id="descripcion"
                  type="Text"
                  title="Ingrese la descripción del Permiso"
                  placeholder="Ingrese la descripción del Permiso"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Registros
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default PerCreatex;
