import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link,useLocation } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../../components/Table';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const PerView = () => {
  const id = useLocation();
  const id_rol = id.search.split('=')[1];
  const [rol, setRol] = useState(null);
  const { authAxios } = useContext(FetchContext);

  const request = async (idrol) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/user/permisosrol/${idrol}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    
    const conID = data.data.map((row) => {
      row.editar = `${row.codigo}`;
      return row;
    });
    data.data=conID
    return data;
  };
  const history = useHistory();
  const [usrs, setUsrs] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project);
  }
  useEffect(() => {
    setIsLoading(true);
    request(id_rol).then((data) => {
      // console.log(data)
      setRol(data.rol);
      setUsrs(data.data);
      setIsLoading(false);
    });
  }, []);

  if (usrs === null) {
    return (
      <div>
        <h1>Permisos del Rol</h1>
        <h3>{rol}</h3>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }
  const handleAdd = () => {
    history.push(`/perAdd?idRol=${id_rol}`);
  };
  const handleBack = () => {
    history.push(`/Roles`);
  };
  const columns = [
    {
      Header: 'Permiso',
      columns: [
        {
          Header: 'Código de Permiso',
          accessor: 'codigo',
        },
        {
          Header: 'Descripción',
          accessor: 'descripcion', 
        },

        {
          Header: 'Grupo',
          accessor: 'grupo',
        },
        {
          Header: 'Descripción Grupo',
          accessor: 'descripcion_grupo',
        },
        {
          Header: 'Descripción Código',
          accessor: 'descripcion_completa_cdg',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/perUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }
  // console.log(proyecto);
  return (
    <div>
      <h1>Permisos del Rol</h1>
      <h3>{rol}</h3>
      {isAdmin() ? (
      <>
      <button
          onClick={() => handleBack()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Regresar
        </button>
        <button
          onClick={() => handleAdd()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Agregar 
        </button>
        
        
      </>
      ) : (
        []
      )}

{isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={usrs && usrs ? usrs : []} columns={columns} />
      )}
    </div>
  );
};

export default PerView;
