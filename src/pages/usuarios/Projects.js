/* eslint-disable react/jsx-indent */
/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useState, useEffect, useContext } from 'react';

import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import '../../static/styles/navbar.scss';
import '../../static/styles/proyectos.scss';
import { FetchContext } from '../../context/FetchContext';

const Projects = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (info) => {
    const proy = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/user/prj/`,
      data: info.proyectos,
    });
    const resps = await proy.data;
    return resps;
  };

  const { setSelPro, authState } = useContext(AuthContext);
  // console.log(authState);
  const history = useHistory();
  const [pro, setPro] = useState(null);

  useEffect(() => {
    request(authState.userInfo).then((data) => {
      setPro(data);
    });
  }, []);

  const handleClick = (ps) => {
    const x = JSON.parse(ps.target.value);
    setSelPro(x);
    history.push('/');
  };

  return (
    <div className="wrap-fluid">
      <div className="container-fluid paper-wrap bevel tlbr">
        <div className="content-wrap">
          <div className="row">
            <h1>Seleccione un Proyecto </h1>
          </div>
          <div className="row">
            <div className="col-sm-12">
              {pro
                ? pro.map((prov) => (
                    <div
                      key={prov.nombre_alterno}
                      className="col-sm-3"
                      style={{
                        float: 'left',
                      }}
                    >
                      <div className="plan">
                        <h3 className="plan-title"> {prov.nombre_alterno} </h3>
                        <ul className="plan-features">
                          <ul className="plan-feature" />
                        </ul>
                        <button
                          value={JSON.stringify(prov)}
                          className="plan-button"
                          onClick={(e) => {
                            handleClick(e);
                          }}
                        >
                          Seleccionar
                        </button>
                      </div>
                    </div>
                  ))
                : []}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Projects;
