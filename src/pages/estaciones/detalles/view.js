import React, { useState, useEffect, useContext } from 'react';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../../components/Table';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const EstCamView = () => {
  const { authAxios } = useContext(FetchContext);
  const request1 = async (idEstacion) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/estaciones/atributos/${idEstacion}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.vigente ? (row.vigente = '1.Si') : (row.vigente = '0.No');
      row.nombre = row.id_item.split('.')[1];
      row.id_item = row.id_item.split('.')[0];
      row.editar = `${row.id_item}E${row.id_estacion}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const id1 = useLocation().search.split('=')[1];
  const idEstacion = id1.split('M')[0];
  // const idMetodologia = id1.split('M')[1];
  const [isLoading, setIsLoading] = useState(false);
  const [est, setEst] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request1(idEstacion).then((data) => {
      setEst(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push(`/Estaciones_Detalles_Create?id=${id1}`);
  };
  const handleBack = () => {
    history.push('/Estaciones_Raiz');
  };

  if (est === null) {
    return (
      <div>
        <h1> Atributos Adicionales de la Estación</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          <Spinner/>
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Campos',
      columns: [
        {
          Header: 'Id Item',
          accessor: 'id_item',
          disableFilters: true,
          disableSorts: true,
        },
        {
          Header: 'Campo adicional',
          accessor: 'nombre',
          disableFilters: true,
        },
        {
          Header: 'Id Estación',
          accessor: 'id_estacion',
          disableFilters: true,
        },
        {
          Header: 'Id Metodología',
          accessor: 'id_metodologia',
          disableFilters: true,
        },
        {
          Header: 'Valor',
          accessor: 'valor',
          disableFilters: true,
        },

        {
          Header: 'Fecha del sistema',
          accessor: 'fsistema',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/Estaciones_Detalles_Update?id=${row.value}`}> ✏ </Link>{' '}
              {/* <button onClick={() => handleEdit(row.value)}>Edit</button> */}{' '}
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1> Atributos Adicionales de la Estación </h1>{' '}
      {isLoading ? (<>  
        <p> Cargando pagina </p>
        <Spinner/>
        </>
      ) : (
        <div>
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          <Table data={est && est ? est : []} columns={columns} nopag />
        </div>
      )}{' '}
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}
    </div>
  );
};

export default EstCamView;
