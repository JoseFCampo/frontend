/* eslint-disable no-console */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useContext } from "react";
import "../../../static/styles/forms.scss";

import { useLocation, useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import "react-datepicker/src/stylesheets/datepicker.scss";
import Message from "../../../components/Message";
import { FetchContext } from "../../../context/FetchContext";
import { AuthContext } from "../../../context/AuthContext";

const EstCamCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto;
  if (project) {
    proyecto = JSON.parse(project);
  }

  const request1 = async (proyecto) => {
    const respuesta = await authAxios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}/estaciones/atributosOpc/${proyecto}`,
    });
    const resps = await respuesta.data;
    return resps;
  };
  const request2 = async (proyecto) => {
    const respuesta = await authAxios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}/proyectos/metxpro/`,
      data: proyecto,
    });
    const resps = await respuesta.data;
    return resps;
  };

  const [resp, setResp] = useState(null);
  const id1 = useLocation().search.split("=")[1];
  const idEstacion = id1.split("M")[0];
  const idMetodologia = id1.split("M")[1];
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const [mets, setMets] = useState(null);
  const [attr, setAttr] = useState([]);
  useEffect(() => {
    if (project) {
      request1(proyecto.codigo).then((data1) => {
        console.log(data1);
        setAttr(data1);
      });
      request2(proyecto).then((data1) => {
        console.log(data1);
        setMets(data1);
      });
    }
  }, []);

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Asignacion de Atributos Adicionales</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: "block",
              }}
            >
              Regresar{" "}
            </button>
            {resp ? <Message response={resp} /> : ""}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            id_estacion: idEstacion,
            id_metodologia: idMetodologia,
            id_item: "-1",
            valor: "",
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: "post",
              url: `${process.env.REACT_APP_API_URL}/estaciones/atributos/create/`,
              data: values,
            });
            setResp(respuesta);
          }}
          validationSchema={Yup.object().shape({
            id_estacion: Yup.number().required("Campo Obligatorio"),
            id_metodologia: Yup.number().nullable(true),
            id_item: Yup.number().min(
              0,
              "No ha seleccionado ninguna opcion"
            ).required("Campo Obligatorio"),
            valor: Yup.string().required("Campo Obligatorio"),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <div class="row">
                  <div class="col">
                    <label
                      style={{
                        display: "block",
                      }}
                    >
                      Identificador de la Estación *
                    </label>
                    <input
                      id="id_estacion"
                      type="Number"
                      title="Identificador de la estación"
                      placeholder="Ingrese el id de la estación. Solo Numeros "
                      disabled
                      value={values.id_estacion}
                      onBlur={handleBlur}
                      className={
                        errors.id_estacion && touched.id_estacion
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.id_estacion && touched.id_estacion && (
                      <div className="input-feedback">{errors.id_estacion}</div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      style={{
                        display: "block",
                      }}
                    >
                      Metodología 
                    </label>
                    <select
                      id="id_metodologia"
                      placeholder="Ingrese el identificador de la Metodología"
                      title="Identificador de la Metodología"
                      type="Number"
                      value={values.id_metodologia}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.id_metodologia && touched.id_metodologia
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value={null} label="Sin Metodologia" />
                      {mets &&
                        mets.map((met) => (
                          <option value={met.id_metodologia}>
                            {" "}
                            {met.id_metodologia} {met.metodologia}
                          </option>
                        ))}
                    </select>

                    {errors.id_metodologia && touched.id_metodologia && (
                      <div className="input-feedback">
                        {errors.id_metodologia}
                      </div>
                    )}
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <label
                      htmlFor="id_item"
                      style={{
                        display: "block",
                      }}
                    >
                    Atributo Adicional *
                    </label>
                    <select
                      id="id_item"
                      title="Ingrese el id el Item, Lista en Campos adicionales"
                      placeholder="Ingrese el Id del item"
                      type="Number"
                      value={values.id_item}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.id_item && touched.id_item
                          ? "text-input error"
                          : "text-input"
                      }
                      disabled={touched.tipo_dato && values.tipo_dato === "4"}
                    >

                      <option value="-1">Seleccione un Atributo </option>
                      {attr&& attr.map(
                        (op)=><option value={parseInt(op.id_item)}> {op.id_item}. {op.descripcion_item}</option>
                      )}

                    </select>
                    {errors.id_item && touched.id_item && (
                      <div className="input-feedback">{errors.id_item}</div>
                    )}
                  </div>

                  {values.id_item!=-1 &&
                  <div class="col">
                    <label
                      htmlFor="valor"
                      style={{
                        display: "block",
                      }}
                    >
                      Valor del atributo *
                    </label>
                    <input
                      id="valor"
                      title="Valor del atributo"
                      placeholder="Ingrese el valor del atributo"
                      type={
                        attr.find((val)=> val.id_item == values.id_item).tipo_dato == 1 ?"Number":"Text" 
                      }
                      value={values.valor}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.valor && touched.valor
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.valor && touched.valor && (
                      <div className="input-feedback">{errors.valor}</div>
                    )}
                  </div>
                    }
                </div>

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Agregar Atributo
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default EstCamCreate;
