/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
import React, { useState, useEffect ,useContext} from "react";
import { useLocation, useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import "react-datepicker/src/stylesheets/datepicker.scss";
import "../../../static/styles/forms.scss";
import Message from "../../../components/Message";
import { FetchContext } from '../../../context/FetchContext';

const EstAtrUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (idEstacion, idItem) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/estaciones/atributos/u/${idEstacion},${idItem}/`,
      {
        crossdomain: true,
      },
    );

    response.data.id_item = response.data.id_item.split(".")[0];
    return response;
  };

  const [atr, setAtr] = useState(null);
  const [resp, setResp] = useState(null);
  const id1 = useLocation().search.split("=")[1];
  const idEstacion = id1.split("E")[1];
  const idItem = id1.split("E")[0];
  useEffect(() => {
    request(idEstacion, idItem).then((data) => {
      setAtr(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (atr === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Atributos...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Atributos</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: "block",
            }}
          >
            Regresar{" "}
          </button>
          {resp ? <Message response={resp} /> : ""}
        </div>
        <br />
        <Formik
          initialValues={{
            id_estacion: atr.id_estacion,
            id_metodologia: atr.id_metodologia,
            id_item: atr.id_item,
            valor: atr.valor,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: "put",
              url: `${process.env.REACT_APP_API_URL}/estaciones/atrpos/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            id_estacion: Yup.number().required('Campo Obligatorio'),
            id_metodologia: Yup.number().required('Campo Obligatorio'),
            id_item: Yup.number().required('Campo Obligatorio'),
            valor: Yup.string().required('Campo Obligatorio'),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  htmlFor="id_item"
                  style={{
                    display: "block",
                  }}
                >
                  Identificador del Atributo
                </label>
                <input
                  id="id_item"
                  title="No se puede modificar"
                  placeholder="Ingrese el Id del item"
                  type="Number"
                  value={values.id_item}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                  className={
                    errors.id_item && touched.id_item
                      ? "text-input error"
                      : "text-input"
                  }
                  disabled={touched.tipo_dato && values.tipo_dato === "4"}
                />
                {errors.id_item && touched.id_item && (
                  <div className="input-feedback">{errors.id_item}</div>
                )}

                <label
                  style={{
                    display: "block",
                  }}
                >
                  Identificador de la Estación
                </label>
                <input
                  id="id_estacion"
                  type="Number"
                  title=" No se puede modificar "
                  placeholder="Ingrese el id de la estación. Solo Numeros "
                  value={values.id_estacion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                  className={
                    errors.id_estacion && touched.id_estacion
                      ? "text-input error"
                      : "text-input"
                  }
                />
                {errors.id_estacion && touched.id_estacion && (
                  <div className="input-feedback">{errors.id_estacion}</div>
                )}

                <label
                  style={{
                    display: "block",
                  }}
                >
                  Identificador de la Metodología
                </label>
                <input
                  id="id_metodologia"
                  placeholder="Ingrese la descripción del item"
                  title="No se puede modificar"
                  type="Number"
                  value={values.id_metodologia}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                  className={
                    errors.id_metodologia && touched.id_metodologia
                      ? "text-input error"
                      : "text-input"
                  }
                />
                {errors.id_metodologia && touched.id_metodologia && (
                  <div className="input-feedback">{errors.id_metodologia}</div>
                )}

                <label
                  htmlFor="valor"
                  style={{
                    display: "block",
                  }}
                >
                  Valor del atributo
                </label>
                <input
                  id="valor"
                  title="Valor del atributo"
                  placeholder="Ingrese el valor del atributo"
                  type="Text"
                  value={values.valor}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.valor && touched.valor
                      ? "text-input error"
                      : "text-input"
                  }
                />
                {errors.valor && touched.valor && (
                  <div className="input-feedback">{errors.valor}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Modificar Atributo
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default EstAtrUpdate;
