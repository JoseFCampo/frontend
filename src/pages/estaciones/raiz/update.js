/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
import React, { useState, useEffect ,useContext} from "react";
import { useLocation, useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import Axios from "axios";
import "react-datepicker/src/stylesheets/datepicker.scss";
import "../../../static/styles/forms.scss";
import Message from "../../../components/Message";
import { FetchContext } from '../../../context/FetchContext';
import { AuthContext } from "../../../context/AuthContext";

const EstRzUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const { project, isAdmin } = useContext(AuthContext);

  let proyecto;
  if (project) {
    proyecto = JSON.parse(project);
  }

  const request = async (idEstacion) => {

    const response = await authAxios.get(`${process.env.REACT_APP_API_URL}/estaciones/raiz/u/${idEstacion}/`,
      {
        crossdomain: true,
      },
    );
    const row = response.data;
    row.nodo_final = row.nodo_final ? "1" : "0";
    row.id_tipo_registro = row.id_tipo_registro ? "1" : "0";
    row.nodo_padre = !row.nodo_padre ? "Null" : row.nodo_padre;
    row.id_metodologia = !row.id_metodologia ? "Null" : row.id_metodologia;
    row.prefijo_estacion = !row.prefijo_estacion ? "Null" : row.prefijo_estacion;
    return row;
  };


  const request1 = async (proyecto) => {
    const respuesta = await authAxios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}/proyectos/metxpro/`,
      data: proyecto,
    });
    const resps = await respuesta.data;
    return resps;
  };

  const request2 = async () => {
    const respuesta = await authAxios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}/estaciones/paises/`,
    });
    const resps = await respuesta.data;
    return resps;
  };
  const request3 = async () => {
    const respuesta = await authAxios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}/estaciones/tipoNodo/`,
    });
    const resps = await respuesta.data;
    return resps;
  };

  const [mets, setMets] = useState(null);
  const [paises, setPais] = useState(null);
  const [tnodo, setTnodo] = useState(null);


  const [rz, setRz] = useState(null);
  const [resp, setResp] = useState(null);
  const id = useLocation();
  const idEstacion = id.search.split("=")[1];
  useEffect(() => {
    request(idEstacion).then((data) => {
      setRz(data);
    });
    if (project) {
      request1(proyecto).then((data1) => {
        // console.log(data1);
        setMets(data1);
      });
    }
    request2().then((data1) => {
      // console.log(data1);
      setPais(data1);
    });
    request3().then((data1) => {
      // console.log(data1);
      setTnodo(data1);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (rz === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Estaciones...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Estaciones</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: "block",
            }}
          >
            Regresar{" "}
          </button>
          {resp ? <Message response={resp} /> : ""}
        </div>
        <br />
        <Formik
          initialValues={{
            id_estacion: rz.id_estacion,
            id_proyecto: proyecto.codigo,
            id_metodologia: rz.id_metodologia,
            id_tipo_registro: rz.id_tipo_registro,
            prefijo_estacion: rz.prefijo_estacion,
            consecutivo_estacion: rz.consecutivo_estacion,
            pais_loc: rz.pais_loc,
            tipo_nodo: rz.tipo_nodo,
            nodo_padre: rz.nodo_padre,
            nodo_final: rz.nodo_final,
            vigente: rz.vigente,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: "put",
              url: `${process.env.REACT_APP_API_URL}/estaciones/estaciones/raiz/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            id_proyecto: Yup.number().required('Campo Obligatorio'),
            id_metodologia: Yup.number().required('Campo Obligatorio'),
            id_tipo_registro: Yup.number(),
            prefijo_estacion: Yup.string().trim(),
            consecutivo_estacion: Yup.number(),
            pais_loc: Yup.string(),
            tipo_nodo: Yup.number().required('Campo Obligatorio'),
            nodo_padre: Yup.number(),
            nodo_final: Yup.number(),
            vigente: Yup.string().trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label htmlFor="id_estacion" style={{ display: "block" }}>
                  Id de la estación
                </label>
                <input
                  id="id_estacion"
                  title="Id del proyecto al que pertenece la estación"
                  placeholder="Ingrese el ID del proyecto"
                  type="Number"
                  value={values.id_estacion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                  className={
                    errors.id_estacion && touched.id_estacion
                      ? "text-input error"
                      : "text-input"
                  }
                />
                {errors.id_estacion && touched.id_estacion && (
                  <div className="input-feedback">{errors.id_estacion}</div>
                )}
<div class="row">
                  <div class="col">
                    <label
                      htmlFor="id_proyecto"
                      style={{
                        display: "block",
                      }}
                    >
                      Proyecto
                    </label>
                    <input
                      id="id_proyecto"
                      title="Id del proyecto al que pertenece la estacion"
                      placeholder="Ingrese el ID del proyecto"
                      type="text"
                      value={proyecto.nombre_alterno}
                      disabled
                      onBlur={handleBlur}
                      className={
                        errors.id_proyecto && touched.id_proyecto
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.id_proyecto && touched.id_proyecto && (
                      <div className="input-feedback">{errors.id_proyecto}</div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      htmlFor="id_metodologia"
                      style={{
                        display: "block",
                      }}
                    >
                      Metodología
                    </label>

                    <select
                      id="id_metodologia"
                      title="Id de la metodología a la que pertenece la estación"
                      placeholder="Ingrese el Id de la metodología"
                      value={values.id_metodologia}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.id_metodologia && touched.id_metodologia
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value={null} label="Sin Metodologia" />

                      {mets &&
                        mets.map((met) => {
                          return (
                            <option
                              value={met.id_metodologia}
                              label={met.metodologia}
                            />
                          );
                        })}
                    </select>

                    {errors.id_metodologia && touched.id_metodologia && (
                      <div className="input-feedback">
                        {errors.id_metodologia}
                      </div>
                    )}
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <label
                      htmlFor="prefijo_estacion"
                      style={{
                        display: "block",
                      }}
                    >
                      Prefijo de la Estación
                    </label>
                    <input
                      id="prefijo_estacion"
                      type="Text"
                      title="Ingrese el encabezado de columna. Max 100 caracteres "
                      placeholder="Ingrese el encabezado de la columna"
                      value={values.prefijo_estacion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.prefijo_estacion && touched.prefijo_estacion
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.prefijo_estacion && touched.prefijo_estacion && (
                      <div className="input-feedback">
                        {errors.prefijo_estacion}
                      </div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      htmlFor="consecutivo_estacion"
                      style={{
                        display: "block",
                      }}
                    >
                      Consecutivo de la estación
                    </label>
                    <input
                      id="consecutivo_estacion"
                      type="Number"
                      title="Ingrese el consecutivo de la estación en el marco de la actividad"
                      placeholder="Ingrese el consecutivo de la estación"
                      value={values.consecutivo_estacion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.consecutivo_estacion &&
                        touched.consecutivo_estacion
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.consecutivo_estacion &&
                      touched.consecutivo_estacion && (
                        <div className="input-feedback">
                          {errors.consecutivo_estacion}
                        </div>
                      )}
                  </div>
                </div>

                <label htmlFor="id_tipo_registro" style={{ display: "block" }}>
                  Tipo de registro
                </label>
                <input
                  id="id_tipo_registro"
                  title="Marcar provenienstes de AG_NODOS = 1/ AG_ESTACIONES= 0"
                  placeholder="Ingrese el Id del tipo de registro"
                  type="Text"
                  value={values.id_tipo_registro}
                  disabled
                  onBlur={handleBlur}
                  className={
                    errors.id_tipo_registro && touched.id_tipo_registro
                      ? "text-input error"
                      : "text-input"
                  }
                />
                {errors.id_tipo_registro && touched.id_tipo_registro && (
                  <div className="input-feedback">
                    {errors.id_tipo_registro}
                  </div>
                )}

                

                <label
                  style={{
                    display: "block",
                  }}
                >
                  Pais
                </label>
                <select
                  id="pais_loc"
                  placeholder="Ingrese el pais (Valores del dominio ISO 3166)"
                  title="Ingrese el pais de la estación"
                  type="text"
                  value={values.pais_loc}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.pais_loc && touched.pais_loc
                      ? "text-input error"
                      : "text-input"
                  }
                >
                  {paises &&
                    paises.map((pais) => (
                      <option value={pais.codigo_pa} label={pais.nombre_pa} />
                    ))}
                </select>
                {errors.pais_loc && touched.pais_loc && (
                  <div className="input-feedback">{errors.pais_loc}</div>
                )}

<label
                  htmlFor="tipo_nodo"
                  style={{
                    display: "block",
                  }}
                >
                  Tipo de nodo
                </label>

                <select
                  id="tipo_nodo"
                  type="Number"
                  title="Ingrese el Tipo de nodo"
                  placeholder="Ingrese el tipo de nodo, Lista en AG_TIPO_NODO, CODIGOS_LOV"
                  value={values.tipo_nodo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.tipo_nodo && touched.tipo_nodo
                      ? "text-input error"
                      : "text-input"
                  }
                >
                  <option value="-1" label="Seleccione el Tipo de Nodo" />
                  {tnodo &&
                    tnodo.map((tnodo) => (
                      <option value={tnodo.codigo} label={tnodo.descripcion} />
                    ))}
                </select>

                {errors.tipo_nodo && touched.tipo_nodo && (
                  <div className="input-feedback">{errors.tipo_nodo}</div>
                )}
<div class="row">
                  <div class="col">
                    <label
                      style={{
                        display: "block",
                      }}
                    >
                      Nodo Final
                    </label>
                    <select
                      id="nodo_final"
                      placeholder="Tipo de Nodo"
                      title="Tipo de Nodo"
                      value={values.nodo_final}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.nodo_final && touched.nodo_final
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value="-1" label="Seleccione respuesta" />
                      <option value="0" label="0. Nodo con hijos" />
                      <option value="1" label="1. Nodo final (hoja)" />
                      <option value="2" label="2. Nodo Raiz" />
                    </select>
                    {errors.nodo_final && touched.nodo_final && (
                      <div className="input-feedback">{errors.nodo_final}</div>
                    )}
                  </div>

                  {(values.nodo_final == "0" || values.nodo_final == "2") ? (
                    <div class="col">
                      <label
                        style={{
                          display: "block",
                        }}
                      >
                        Id del nodo padre de la estación
                      </label>
                      <input
                        id="nodo_padre"
                        type="Number"
                        title="Ingrese el Id del nodo padre de la estación si es "
                        placeholder="Ingrese el Id del nodo padre de la estación"
                        value={values.nodo_padre}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          errors.nodo_padre && touched.nodo_padre
                            ? "text-input error"
                            : "text-input"
                        }
                      />
                      {errors.nodo_padre && touched.nodo_padre && (
                        <div className="input-feedback">
                          {errors.nodo_padre}
                        </div>
                      )}
                    </div>
                  ): values.nodo_padre=null}
                </div>

                <label htmlFor="vigente" style={{ display: "block" }}>
                  La estación aun esta vigente
                </label>
                <select
                  id="vigente"
                  placeholder="Esta vigente?"
                  title="Vigencia"
                  value={values.vigente}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.vigente && touched.vigente
                      ? "text-input error"
                      : "text-input"
                  }
                >
                  <option value="" label="Seleccione respuesta" />
                  <option value="N" label="0. NO" />
                  <option value="S" label="1. SI" />
                </select>
                {errors.vigente && touched.vigente && (
                  <div className="input-feedback">{errors.vigente}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Modificar Campo
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default EstRzUpdate;
