import React, { useState, useEffect, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../../components/Table';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';

const EstRzView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (proyecto) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/estaciones/raiz/${proyecto}`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_estacion}`;
      row.detalle = `${row.id_estacion}M${row.id_metodologia}`;
      row.nodo_final = row.nodo_final==1 ? 'Nodo con Hijos' : row.nodo_final==2 ?'Nodo Raiz':'Nodo Final';
      row.id_tipo_registro = row.id_tipo_registro ? '1' : '0';
      row.nodo_padre = !row.nodo_padre ? 'Sin Padre' : row.nodo_padre;
      row.id_metodologia = !row.id_metodologia ? ' ' : row.id_metodologia;
      row.prefijo_estacion = !row.prefijo_estacion ? ' ' : row.prefijo_estacion;

      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [rz, setRz] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto;
  if (project) {
    proyecto = JSON.parse(project);
    console.log(proyecto)
  }
  useEffect(() => {
    setIsLoading(true);
    request(proyecto.codigo).then((data) => {
      setRz(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/Estaciones_Raiz_Create');
  };

  if (rz === null) {
    return (
      <div>
        <h1> Informacion Base de las Estaciones </h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...
          <Spinner/>
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Estaciones',
      columns: [
        {
          Header: 'Id',
          accessor: 'id_estacion',
          filter: 'equals',
          width: 50,
        },
        // {
        //   Header: 'Id Proyecto',
        //   accessor: 'id_proyecto',
        //   Filter: SelectColumnFilter,
        //   filter: 'equals',
        // },
        {
          Header: 'Id Metodología',
          accessor: 'metodologia',
          Filter: SelectColumnFilter,
          filter: 'equals',
        },
        // {
        //   Header: 'Id tipo de Registro',
        //   accessor: 'id_tipo_registro',
        //   Filter: SelectColumnFilter,
        //   filter: 'equals',
        // },
        {
          Header: 'Prefijo',
          accessor: 'prefijo_estacion',
        },
        {
          Header: 'Consecutivo',
          accessor: 'consecutivo_estacion',
        },
        {
          Header: 'Pais',
          accessor: 'pais_loc',
          Filter: SelectColumnFilter,
          filter: 'equals',
        },
        {
          Header: 'Tipo de Nodo',
          accessor: 'tipo_nodo',
          Filter: SelectColumnFilter,
          filter: 'equals',
        },
        {
          Header: 'Nodo Padre',
          accessor: 'nodo_padre',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Nodo Final',
          accessor: 'nodo_final',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Vigente',
          accessor: 'vigente',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Fecha de registro',
          accessor: 'fsistema',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/Estaciones_Raiz_Update?id=${row.value}`}> ✏ </Link>{' '}
            </div>
          ),
        },
        {
          Header: 'Detalle',
          width: 70,
          disableFilters: true,
          accessor: 'detalle',
          Cell: (row) => (
            <div>
              <Link to={`/Estaciones_Detalles?id=${row.value}`}>🔍</Link>{' '}
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1> Informacion Base de las Estaciones  </h1>{' '}
      <h2>{proyecto.titulo}</h2>{' '}
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}
      {isLoading ? (
        <>
        <p> Cargando pagina </p>
        <Spinner/>
        </>
      ) : (
        <Table data={rz && rz ? rz : []} columns={columns} />
      )}{' '}
    </div>
  );
};

export default EstRzView;
