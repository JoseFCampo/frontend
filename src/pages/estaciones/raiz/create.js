/* eslint-disable no-console */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useContext } from "react";
import "../../../static/styles/forms.scss";

import { useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";

import DatePicker from "../../../components/DatePicker";
import "react-datepicker/src/stylesheets/datepicker.scss";
import Message from "../../../components/Message";
import { FetchContext } from "../../../context/FetchContext";
import { AuthContext } from "../../../context/AuthContext";

const EstRzCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto;
  if (project) {
    proyecto = JSON.parse(project);
  }
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  const request1 = async (proyecto) => {
    const respuesta = await authAxios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}/proyectos/metxpro/`,
      data: proyecto,
    });
    const resps = await respuesta.data;
    return resps;
  };

  const request2 = async () => {
    const respuesta = await authAxios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}/estaciones/paises/`,
    });
    const resps = await respuesta.data;
    return resps;
  };
  const request3 = async (proyecto) => {
    const respuesta = await authAxios({
      method: "get",
      url: `${process.env.REACT_APP_API_URL}/estaciones/tipoNodo/${proyecto}`,
    });
    const resps = await respuesta.data;
    return resps;
  };
  const request4 = async (tipoNodo, proyecto) => {
    const respuesta = await authAxios({
      method: "post",
      url: `${process.env.REACT_APP_API_URL}/estaciones/NodoPadre/`,
      data: {
        proyecto,
        tipoNodo,
      },
    });
    const resps = await respuesta.data;
    return resps;
  };

  const [mets, setMets] = useState(null);
  const [metd, setMetd] = useState(null);
  const [paises, setPais] = useState(null);
  const [tnodo, setTnodo] = useState(null);
  const [nodoPadre, setNodoPadre] = useState(0);
  useEffect(() => {
    if (project) {
      request1(proyecto).then((data1) => {
        // console.log(data1);
        setMets(data1);
      });
    }
    request2().then((data1) => {
      // console.log(data1);
      setPais(data1);
    });
    request3(proyecto.codigo).then((data1) => {
      // console.log(data1);
      setTnodo(data1);
    });
  }, []);

  const handleChangeNodo = (e) => {
    // console.log(e.target.value);
    let ordenPadre = tnodo.findIndex((tn) => e.target.value == tn.codigo);
    if (ordenPadre + 1 < tnodo.length) {
      request4(tnodo[ordenPadre + 1].codigo, proyecto.codigo).then((data1) => {
        // console.log(data1);
        setNodoPadre(data1);
      });
      if (ordenPadre == 0) {
        return 1;
      } else {
        return 0;
      }
    } else {
      setNodoPadre(0);
      return 2;
    }
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Estaciones</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: "block",
              }}
            >
              Regresar{" "}
            </button>
            {resp && <Message response={resp} />}
            {resp
              ? window.setTimeout(() => {
                  history.push(
                    `/Estaciones_Detalles?id=${resp.data.id_estacion}M${metd}`
                  );
                }, 1000)
              : ""}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            id_proyecto: proyecto.codigo,
            id_metodologia: null,
            id_tipo_registro: "-1",
            prefijo_estacion: "",
            consecutivo_estacion: "",
            pais_loc: "CO",
            tipo_nodo: "-1",
            nodo_padre: "0",
            nodo_final: "-1",
            vigente: "-1",
          }}
          onSubmit={async (values) => {
            console.log(values);
            const respuesta = await authAxios({
              method: "post",
              url: `${process.env.REACT_APP_API_URL}/estaciones/raiz/create/`,
              data: values,
            });
            setResp(respuesta);
          }}
          validationSchema={Yup.object().shape({
            id_metodologia: Yup.number().nullable(true),
            id_tipo_registro: Yup.number().min(
              0,
              "No ha seleccionado ninguna opcion"
            ),
            prefijo_estacion: Yup.string().trim().required("Campo Obligatorio"),
            consecutivo_estacion: Yup.number()
              .max(99999, "Numero de Caracteres Maximos: 5"),
            pais_loc: Yup.string().required("Campo Obligatorio"),
            tipo_nodo: Yup.number().min(
              0,
              "No ha seleccionado el tipo de Nodo"
            ),
            nodo_padre: Yup.number().min(0, "No ha seleccionado el Nodo Padre"),
            nodo_final: Yup.number().min(
              0,
              "No ha seleccionado ninguna opcion"
            ),
            vigente: Yup.string().required("Campo Obligatorio"),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;

            return (
              <form onSubmit={handleSubmit}>
                <div class="row">
                  <div class="col">
                    <label
                      htmlFor="id_proyecto"
                      style={{
                        display: "block",
                      }}
                    >
                      Proyecto
                    </label>
                    <input
                      id="id_proyecto"
                      title="Id del proyecto al que pertenece la estacion"
                      placeholder="Ingrese el ID del proyecto"
                      type="text"
                      value={proyecto.nombre_alterno}
                      disabled
                      onBlur={handleBlur}
                      className={
                        errors.id_proyecto && touched.id_proyecto
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.id_proyecto && touched.id_proyecto && (
                      <div className="input-feedback">{errors.id_proyecto}</div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      htmlFor="id_metodologia"
                      style={{
                        display: "block",
                      }}
                    >
                      Metodología
                    </label>

                    <select
                      id="id_metodologia"
                      title="Id de la metodología a la que pertenece la estación"
                      placeholder="Ingrese el Id de la metodología"
                      value={values.id_metodologia}
                      onChange={(e) => {
                        handleChange(e),
                        setMetd(e.target.value);
                      }}
                      onBlur={handleBlur}
                      className={
                        errors.id_metodologia && touched.id_metodologia
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value={null} label="Sin Metodologia" />

                      {mets &&
                        mets.map((met) => {
                          return (
                            <option
                              value={met.id_metodologia}
                              label={met.metodologia}
                            />
                          );
                        })}
                    </select>

                    {errors.id_metodologia && touched.id_metodologia && (
                      <div className="input-feedback">
                        {errors.id_metodologia}
                      </div>
                    )}
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <label
                      htmlFor="prefijo_estacion"
                      style={{
                        display: "block",
                      }}
                    >
                      Prefijo de la Estación
                    </label>
                    <input
                      id="prefijo_estacion"
                      type="Text"
                      title="Ingrese el encabezado de columna. Max 100 caracteres "
                      placeholder="Ingrese el encabezado de la columna"
                      value={values.prefijo_estacion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.prefijo_estacion && touched.prefijo_estacion
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.prefijo_estacion && touched.prefijo_estacion && (
                      <div className="input-feedback">
                        {errors.prefijo_estacion}
                      </div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      htmlFor="consecutivo_estacion"
                      style={{
                        display: "block",
                      }}
                    >
                      Consecutivo de la estación
                    </label>
                    <input
                      id="consecutivo_estacion"
                      type="Number"
                      title="Ingrese el consecutivo de la estación en el marco de la actividad"
                      placeholder="Ingrese el consecutivo de la estación"
                      value={values.consecutivo_estacion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.consecutivo_estacion &&
                        touched.consecutivo_estacion
                          ? "text-input error"
                          : "text-input"
                      }
                    />
                    {errors.consecutivo_estacion &&
                      touched.consecutivo_estacion && (
                        <div className="input-feedback">
                          {errors.consecutivo_estacion}
                        </div>
                      )}
                  </div>
                </div>

                <label
                  style={{
                    display: "block",
                  }}
                >
                  Pais
                </label>
                <select
                  id="pais_loc"
                  placeholder="Ingrese el pais (Valores del dominio ISO 3166)"
                  title="Ingrese el pais de la estación"
                  type="text"
                  value={values.pais_loc}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.pais_loc && touched.pais_loc
                      ? "text-input error"
                      : "text-input"
                  }
                >
                  {paises &&
                    paises.map((pais) => (
                      <option value={pais.codigo_pa} label={pais.nombre_pa} />
                    ))}
                </select>
                {errors.pais_loc && touched.pais_loc && (
                  <div className="input-feedback">{errors.pais_loc}</div>
                )}

                <div class="row">
                  <div class="col">
                    <label
                      htmlFor="tipo_nodo"
                      style={{
                        display: "block",
                      }}
                    >
                      Tipo de nodo Geografico
                    </label>

                    <select
                      id="tipo_nodo"
                      type="Number"
                      title="Ingrese el Tipo de nodo"
                      placeholder="Ingrese el tipo de nodo, Lista en AG_TIPO_NODO, CODIGOS_LOV"
                      value={values.tipo_nodo}
                      onChange={(e) => {
                        if (tnodo.length > 1) {
                          values.nodo_final = handleChangeNodo(e);
                          values.id_tipo_registro = "1";
                          handleChange(e);
                        } else {
                          values.nodo_padre = "0";
                          values.nodo_final = "1";
                          values.id_tipo_registro = "0";
                          handleChange(e);
                          setNodoPadre(0);
                        }
                      }}
                      onBlur={handleBlur}
                      className={
                        errors.tipo_nodo && touched.tipo_nodo
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option
                        value="-1"
                        label="Seleccione el Tipo de Nodo"
                        disabled
                      />
                      {tnodo &&
                        tnodo.map((tnodo) => (
                          <option value={tnodo.codigo}>
                            {tnodo.codigo} - {tnodo.descripcion}{" "}
                          </option>
                        ))}
                    </select>

                    {errors.tipo_nodo && touched.tipo_nodo && (
                      <div className="input-feedback">{errors.tipo_nodo}</div>
                    )}
                  </div>
                  <div class="col">
                    <label
                      style={{
                        display: "block",
                      }}
                    >
                      Nodo Final
                    </label>
                    <select
                      id="nodo_final"
                      placeholder="Tipo de Nodo"
                      title="Tipo de Nodo"
                      value={values.nodo_final}
                      disabled
                      onBlur={handleBlur}
                      className={
                        errors.nodo_final && touched.nodo_final
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value="-1" label="Seleccione respuesta" />
                      <option value="0" label="0. Nodo con hijos" />
                      <option value="1" label="1. Nodo final (hoja)" />
                      <option value="2" label="2. Nodo Raiz" />
                    </select>
                    {errors.nodo_final && touched.nodo_final && (
                      <div className="input-feedback">{errors.nodo_final}</div>
                    )}
                  </div>

                  <div class="col">
                    <label
                      htmlFor="id_tipo_registro"
                      style={{
                        display: "block",
                      }}
                    >
                      Tipo de registro
                    </label>
                    <select
                      id="id_tipo_registro"
                      title="Marcar provenientes de AG_NODOS = 1/ AG_ESTACIONES= 0"
                      placeholder="Ingrese el Id del tipo de registro"
                      type="Number"
                      value={values.id_tipo_registro}
                      disabled
                      onBlur={handleBlur}
                      className={
                        errors.id_tipo_registro && touched.id_tipo_registro
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value="-1" label="Seleccione respuesta" />
                      <option value="0" label="0. AG_ESTACIONES" />
                      <option value="1" label="1. AG_NODOS" />
                    </select>

                    {errors.id_tipo_registro && touched.id_tipo_registro && (
                      <div className="input-feedback">
                        {errors.id_tipo_registro}
                      </div>
                    )}
                  </div>
                </div>
                {nodoPadre != 0 && (
                  <div class="row" style={{ margin: "1%" }}>
                    <label
                      style={{
                        display: "block",
                      }}
                    >
                      Nodo padre
                    </label>
                    <select
                      id="nodo_padre"
                      type="Number"
                      title="Ingrese el Id del nodo padre de la estación si es "
                      placeholder="Ingrese el Id del nodo padre de la estación"
                      value={values.nodo_padre}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      style={{
                        overflow: "scroll !important",
                        overflowX: "scroll !important",
                        width: "100%",
                      }}
                      className={
                        errors.nodo_padre && touched.nodo_padre
                          ? "text-input error"
                          : "text-input"
                      }
                    >
                      <option value="-1" disabled>
                        Seleccione el padre
                      </option>
                      {nodoPadre.map((nod) => (
                        <option value={nod.id_estacion}>
                          {nod.id_estacion} {nod.path_lugar}
                        </option>
                      ))}
                    </select>
                    {errors.nodo_padre && touched.nodo_padre && (
                      <div className="input-feedback">{errors.nodo_padre}</div>
                    )}
                  </div>
                )}

                <label
                  htmlFor="vigente"
                  style={{
                    display: "block",
                  }}
                >
                  La estación aun esta vigente
                </label>
                <select
                  id="vigente"
                  placeholder="Esta vigente?"
                  title="Vigencia"
                  value={values.vigente}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.vigente && touched.vigente
                      ? "text-input error"
                      : "text-input"
                  }
                >
                  <option value="N" label="Seleccione respuesta" disabled />
                  <option value="N" label="0. NO" />
                  <option value="S" label="1. SI" />
                </select>
                {errors.vigente && touched.vigente && (
                  <div className="input-feedback">{errors.vigente}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear estación
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default EstRzCreate;
