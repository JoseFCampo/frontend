import React, { useState, useEffect, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import { AuthContext } from '../../../context/AuthContext';
import { FetchContext } from '../../../context/FetchContext';
import Table, { SelectColumnFilter } from '../../../components/Table';

const EstCamView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/estaciones/campos/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_item}`;
      row.vigente ? (row.vigente = '1.Si') : (row.vigente = '0.No');
      return row;
    });
    return conID;
  };

  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [est, setEst] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setEst(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/Estaciones_Campos_Create');
  };

  if (est === null) {
    return (
      <div>
        <h1> Atributos Adicionales de las Estaciones</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Campos',
      columns: [
        {
          Header: 'Id',
          accessor: 'id_item',
          width: 100,
        },
        {
          Header: 'Encabezado de la columna',
          accessor: 'columna_encabezado',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Tipo de Dato',
          accessor: 'tipo_dato',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Tabla',
          accessor: 'tabla',
        },
        {
          Header: 'Conjunto',
          accessor: 'conjunto',
        },
        {
          Header: 'Vigente',
          accessor: 'vigente',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Descripción del Item',
          accessor: 'descripcion_item',
        },
        {
          Header: 'Grupo',
          accessor: 'grupo',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Orden',
          accessor: 'orden',
          disableFilters: true,
        },

        {
          Header: 'Fecha del sistema',
          accessor: 'fsistema',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/Estaciones_Campos_Update?id=${row.value}`}> ✏ </Link>{' '}
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1> Campos Adicionales de las Estaciones </h1>{' '}
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}
      {isLoading ? (<>
        <p> Cargando Tabla </p>
        <Spinner/>
        </>
      ) : (
        <Table data={est && est ? est : []} columns={columns} />
      )}{' '}
    </div>
  );
};

export default EstCamView;
