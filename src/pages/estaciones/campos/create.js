/* eslint-disable no-console */
/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react';
import '../../../static/styles/forms.scss';

import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup'
import 'react-datepicker/src/stylesheets/datepicker.scss';
import Message from '../../../components/Message';
import { FetchContext } from '../../../context/FetchContext';

const EstCamCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Atributos Adicionales</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            columna_encabezado: '',
            tipo_dato: '',
            tabla: '',
            conjunto: '',
            vigente: '',
            descripcion_item: '',
            orden: '',
            grupo: '',
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/estaciones/campos/create/`,
              data: values,
            });
            setResp(respuesta);
          }}
          validationSchema={Yup.object().shape({
            columna_encabezado: Yup.string().required('Campo Obligatorio').trim(),
            tipo_dato: Yup.number().required('Campo Obligatorio'),
            tabla: Yup.string(),
            conjunto: Yup.string(),
            vigente: Yup.string().required('Campo Obligatorio'),
            descripcion_item: Yup.string().required('Campo Obligatorio').trim(),
            orden: Yup.number(),
            grupo: Yup.number(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Encabezado de la Columna
                </label>
                <input
                  id="columna_encabezado"
                  type="Text"
                  title="Ingrese el encabezado de columna. Max 100 caracteres "
                  placeholder="Ingrese el encabezado de la columna"
                  value={values.columna_encabezado}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.columna_encabezado && touched.columna_encabezado
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.columna_encabezado && touched.columna_encabezado && (
                  <div className="input-feedback">{errors.columna_encabezado}</div>
                )}

                <label
                  htmlFor="tipo_dato"
                  style={{
                    display: 'block',
                  }}
                >
                  Tipo de Dato
                </label>
                <select
                  id="tipo_dato"
                  placeholder="Seleccione el Tipo de dato"
                  title="Tipo de dato del parámetro. Las opciones se encuentran en datosdecampo.AG_LOV, tabla = 1000 "
                  value={values.tipo_dato}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.tipo_dato && touched.tipo_dato
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  <option value="" label="Tipo de dato" />
                  <option value="1" label="1.Numerico" />
                  <option value="2" label="2.Fecha" />
                  <option value="3" label="3.Alfanumerico" />
                  <option value="4" label="4.Lista proveniente de AG_LOV" />
                </select>
                {errors.tipo_dato && touched.tipo_dato && (
                  <div className="input-feedback">{errors.tipo_dato}</div>
                )}

                <label
                  htmlFor="tabla"
                  style={{
                    display: 'block',
                  }}
                >
                  Numero de tabla
                </label>
                <input
                  id="tabla"
                  title="El Nombre de la tabla en AGLOV de donde vienen los dominios o referentes"
                  placeholder="Ingrese el nombre de la tabla"
                  type="Text"
                  value={values.tabla}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.tabla && touched.tabla ? 'text-input error' : 'text-input'
                  }
                  disabled={!(touched.tipo_dato && values.tipo_dato === '4')}
                />
                {errors.tabla && touched.tabla && (
                  <div className="input-feedback">{errors.tabla}</div>
                )}

                <label
                  htmlFor="conjunto"
                  style={{
                    display: 'block',
                  }}
                >
                  Conjunto de descripciónes y valores
                </label>
                <input
                  id="conjunto"
                  title="Para datos provenientes de listas indica el conjunto que contiene las descripciónes y los valores declarados en la tabla codigos_lov"
                  placeholder="Ingrese el numero del conjunto"
                  type="Number"
                  value={values.conjunto}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.conjunto && touched.conjunto
                      ? 'text-input error'
                      : 'text-input'
                  }
                  disabled={!(touched.tipo_dato && values.tipo_dato === '4')}
                />
                {errors.conjunto && touched.conjunto && (
                  <div className="input-feedback">{errors.conjunto}</div>
                )}

                <label
                  htmlFor="vigente"
                  style={{
                    display: 'block',
                  }}
                >
                  El campo es de interes?
                </label>
                <select
                  id="vigente"
                  placeholder="Es vigente?"
                  title="Vigencia"
                  value={values.vigente}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.vigente && touched.vigente
                      ? 'text-input error'
                      : 'text-input'
                  }
                >
                  <option value="" label="Seleccione respuesta" />
                  <option value="0" label="0. NO" />
                  <option value="1" label="1. SI" />
                </select>
                {errors.vigente && touched.vigente && (
                  <div className="input-feedback">{errors.vigente}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del item
                </label>
                <textarea
                  id="descripcion_item"
                  placeholder="Ingrese la descripción del item"
                  title="En este campo va la descripción del item, Max: 1000 Caracteres. "
                  type="Text"
                  maxLength="1000"
                  value={values.descripcion_item}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion_item && touched.descripcion_item
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion_item && touched.descripcion_item && (
                  <div className="input-feedback">{errors.descripcion_item}</div>
                )}

                <label
                  htmlFor="grupo"
                  style={{
                    display: 'block',
                  }}
                >
                  Grupo
                </label>
                <select
                  id="grupo"
                  placeholder="Grupo"
                  title="Separa los items de acuerdo a su perfil: información geografica, del ambiente, de la campaña."
                  value={values.grupo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.grupo && touched.grupo ? 'text-input error' : 'text-input'
                  }
                >
                  <option value="" label="Seleccione un grupo" />
                  <option value="1" label="1.Dato geografico" />
                  <option value="2" label="2.dato administrativo" />
                  <option value="3" label="3.fisicoquimico" />
                  <option value="4" label="4.Biologico" />
                  <option value="5" label="5.Geologico" />
                  <option value="8" label="8.Atribuutos SIGMA NODOS_GEOGRAFICOS" />
                </select>
                {errors.grupo && touched.grupo && (
                  <div className="input-feedback">{errors.grupo}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Orden en el grupo
                </label>
                <input
                  id="orden"
                  type="Number"
                  title="Ingrese el orden en el que se despliega el item en el grupo"
                  placeholder="Ingrese el orden en el que se despliega el item en el grupo"
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Campo
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default EstCamCreate;
