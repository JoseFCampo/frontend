import React, { useState, useEffect, useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import '../static/styles/navbar.scss';
import SelProyecto from './usuarios/Projects';
import '../static/styles/proyectos.scss';
import { FetchContext } from '../context/FetchContext';

const Home = () => {
  const { authAxios } = useContext(FetchContext);
  const request1 = async (proyecto) => {
    const respuesta = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/proyectos/metxpro/`,
      data: proyecto,
    });
    const resps = await respuesta.data;
    return resps;
  };

  const { project } = useContext(AuthContext);
  const [data, setData] = useState(null);
  useEffect(() => {
    if (project) {
      const proyecto = JSON.parse(project);
      request1(proyecto).then((data1) => {
        // console.log(data1);
        setData(data1);
      });
    }
  }, []);
  if (project) {
    const proyecto = JSON.parse(project);
    return (
      <>
        <div className="wrap-fluid">
          <div className="container-fluid paper-wrap bevel tlbr">
            <div className="content-wrap">
              <div className="col-sm-12">
                <div className="row">
                  <div className="col-sm-6">
                    <div className="nest">
                      <div className="title-alt">
                        <h5
                          style={{
                            padding: '12px',
                          }}
                        >
                          Proyecto
                        </h5>
                      </div>
                      <div
                        className="body-nest"
                        style={{
                          minHeight: '300px',
                        }}
                      >
                        <h1>{proyecto.nombre_alterno}</h1>
                        <dl className="dl-horizontal-profile">
                          <dt>Titulo</dt>
                          <dd>{proyecto.titulo}</dd>
                          <br />
                          {proyecto.finicio ? (
                            <>
                              <dt>Fecha Inicio</dt>
                              <dd>{proyecto.finicio}</dd>
                              <br />
                            </>
                          ) : (
                            []
                          )}

                          {proyecto.ffinalizo ? (
                            <>
                              <dt>Fecha Fin</dt>
                              <dd>{proyecto.ffinalizo}</dd>
                              <br />
                            </>
                          ) : (
                            []
                          )}
                        </dl>
                        <dt>Código del Proyecto</dt>
                        <dd>{proyecto.codigo}</dd>
                        <br />
                      </div>
                    </div>
                  </div>
                  <div
                    className="col-sm-6"
                    style={{
                      float: 'left',
                    }}
                  >
                    <div className="nest">
                      <div className="title-alt">
                        <h5
                          style={{
                            padding: '12px',
                          }}
                        >
                          {' '}
                          Metodologías
                        </h5>
                      </div>
                      <div className="body-nest">
                        <div className="table-responsive">
                          {data ? (
                            <>
                              <table
                                className="table"
                                style={{
                                  color: '#9ea7b3',
                                }}
                              >
                                <thead>
                                  <tr>
                                    <th>Id</th>
                                    <th>Metodologías</th>
                                    <th>Temáticas</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {data.map((metodologia, key) => (
                                    <tr key={key}>
                                      <td className="armada-devider">
                                        <div className="armada">
                                          <dd key>{metodologia.id_metodologia}</dd>
                                        </div>
                                      </td>
                                      <td className="armada-devider">
                                        <div className="armada">
                                          <dd key>{metodologia.metodologia}</dd>
                                        </div>
                                      </td>
                                      <td className="driver-devider">
                                        {metodologia.tematicas_metodologias
                                          ? metodologia.tematicas_metodologias.map(
                                              (tematica, key1) => (
                                                <dd key={key1}>{tematica}</dd>
                                              ),
                                            )
                                          : []}
                                      </td>
                                    </tr>
                                  ))}
                                </tbody>
                              </table>
                            </>
                          ) : (
                            []
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
  return <SelProyecto />;
};
export default Home;
