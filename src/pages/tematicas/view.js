import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const TemView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(`${process.env.REACT_APP_API_URL}/tematicas/`, {
      crossdomain: true,
    });
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_tematicas}`;
      return row;
    });
    return conID;
  };

  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [tem, setTem] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setTem(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/temCreate');
  };

  if (tem === null) {
    return (
      <div>
        <h1>Temáticas</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Temáticas',
      columns: [
        {
          Header: 'Identificador de la Tematica',
          accessor: 'id_tematicas',
          filter: 'equals',
          width: 100,
        },
        {
          Header: 'Descripción de la Tematica',
          accessor: 'descripcion',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Orden',
          accessor: 'orden',
        },
        {
          Header: 'Esquema de la Tematica',
          accessor: 'esquema',
          Filter: SelectColumnFilter,
          filter: 'equals',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/temUpdate?id=${row.value}`}> ✏ </Link>
              {/* <button onClick={() => handleEdit(row.value)}>Edit</button> */}
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Temáticas</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

{isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={tem && tem ? tem : []} columns={columns} />
      )}
    </div>
  );
};

export default TemView;
