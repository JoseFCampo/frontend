/* eslint-disable no-console */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const TemCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Temáticas</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            descripcion: '',
            raiz: '',
            orden: '',
            esquema: '',
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/tematicas/create/`,
              data: values,
            });
            setResp(respuesta);

            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            esquema: Yup.string().trim(),
            raiz: Yup.number().required('Campo Obligatorio'),
            orden: Yup.number().required('Campo Obligatorio'),
            descripcion: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador del padre de la Tematica
                </label>
                <input
                  id="raiz"
                  type="Number"
                  title="Ingrese el identificador del padre de la tematica si no tiene padre coloque 0 "
                  placeholder="Ingrese el identificador del padre de la tematica"
                  value={values.raiz}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.raiz && touched.raiz ? 'text-input error' : 'text-input'
                  }
                />
                {errors.raiz && touched.raiz && (
                  <div className="input-feedback">{errors.raiz}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la Tematica
                </label>
                <textarea
                  id="descripcion"
                  placeholder="Ingrese la descripción de la tematica"
                  title="En este campo va la descripción de la tematica, Max: 1000 Caracteres. "
                  type="Text"
                  maxLength="1000"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Orden jerárquico de la Tematica
                </label>
                <input
                  id="orden"
                  type="Number"
                  title="Ingrese el orden jerárquico de la tematica"
                  placeholder="Ingrese el orden jerárquico de la tematica"
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Esquema de la Tematica
                </label>
                <input
                  id="esquema"
                  type="Text"
                  title="Ingrese el esquema de la tematica. Max 100 caracteres "
                  placeholder="Ingrese el esquema de la tematica"
                  value={values.esquema}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.esquema && touched.esquema
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.esquema && touched.esquema && (
                  <div className="input-feedback">{errors.esquema}</div>
                )}
                <br />

                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>

                <button type="submit" disabled={isSubmitting}>
                  Crear Tematica
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default TemCreate;
