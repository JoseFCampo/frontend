/* eslint-disable jsx-a11y/no-onchange */
/* eslint-disable no-shadow */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable object-curly-newline */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect, useContext } from 'react';
import DatePicker from 'react-datepicker';
import '../../static/styles/forms.scss';
import FilteredMultiSelect from 'react-filtered-multiselect';
import { useTable, useSortBy, usePagination } from 'react-table';
import moment from 'moment';
import { Spinner } from 'react-bootstrap';
import { Styles } from '../../components/Table';
import Excel from '../../components/Export_excel';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const Tabla = ({ columns, data, adi }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      disableMultiSort: false,
    },
    useSortBy,
    usePagination,
  );

  const pageSizeOptions = [10, 20, 50, 100];
  return (
    <div
      style={{
        padding: '2px',
      }}
    >
      <>
        <div
          className="table-responsive"
          style={{
            maxWidth: screen.width - 500,
          }}
        >
          <table className="table table-bordered table-hover" {...getTableProps()}>
            <thead>
              {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <th scope="col" {...column.getHeaderProps()}>
                      <span {...column.getSortByToggleProps()}>
                        {column.render('Header')}
                        {/* Add a sort direction indicator */}
                        {column.isSorted
                          ? column.isSortedDesc
                            ? '   🔽'
                            : '   🔼'
                          : ''}
                      </span>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {page.map((row, i) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => (
                      <td {...cell.getCellProps()}>
                        <div>{cell.render('Cell')}</div>
                      </td>
                    ))}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <table className="table table-hover">
          <tbody>
            <tr>
              <th scope="row">Total de Estaciones Consultadas</th>
              <td>{adi.Count_estaciones}</td>
            </tr>
            <tr>
              <th scope="row">Total de Registros Obtenidos</th>
              <td>{adi.Total_registros}</td>
            </tr>
            <tr>
              <th scope="row">Total de Datos almacenados</th>
              <td>{adi.Total_tabla}</td>
            </tr>
          </tbody>
        </table>
      </>
      <Styles>
        <button
          className="btn btn btn-block btn-primary"
          onClick={() => gotoPage(0)}
          disabled={!canPreviousPage}
        >
          {'<<'}
        </button>{' '}
        <button
          className="btn btn btn-block btn-primary"
          onClick={() => previousPage()}
          disabled={!canPreviousPage}
        >
          {'<'}
        </button>{' '}
        <span>
          Pagina{' '}
          <strong>
            {pageIndex + 1} de {pageOptions.length}
          </strong>{' '}
        </span>
        <span>| Ir a la pagina: </span>{' '}
        <input
          type="number"
          defaultValue={pageIndex + 1}
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          style={{
            width: '100px',
          }}
        />{' '}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {pageSizeOptions.map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Mostrar {pageSize} Filas
            </option>
          ))}
        </select>{' '}
        <button
          className="btn btn btn-block btn-primary"
          onClick={() => nextPage()}
          disabled={!canNextPage}
        >
          {'>'}
        </button>{' '}
        <button
          className="btn btn btn-block btn-primary"
          onClick={() => gotoPage(pageCount - 1)}
          disabled={!canNextPage}
        >
          {'>>'}
        </button>{' '}
      </Styles>
    </div>
  );
};

const CtrView = () => {
  const { authAxios } = useContext(FetchContext);
  const request1 = async () => {
    const proy = await authAxios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/control/proy/`,
      crossdomain: true,
    });
    const resps = await proy.data;
    return resps;
  };
  const request2 = async (id_proyecto) => {
    const respuesta = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/control/proy/`,
      data: {
        id_proyecto,
      },
    });
    const resps = await respuesta.data;
    return resps;
  };

  const request3 = async (
    id_proyecto,
    id_metodologia,
    fecha_inicio,
    fecha_final,
  ) => {
    const respuesta = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/control/est/`,
      data: {
        id_proyecto,
        id_metodologia,
        fecha_inicio: moment(fecha_inicio).format('YYYY-MM'),
        fecha_final: moment(fecha_final).format('YYYY-MM'),
      },
    });
    const resps = await respuesta.data;

    return resps;
  };
  const request4 = async (
    prosel,
    metsel,
    startDate,
    endDate,
    selectedOptions,
    selectedColumns,
    adicionales,
  ) => {
    const respuesta = await authAxios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/control/data/`,
      data: {
        id_proyecto: prosel,
        id_metodologia: metsel,
        fecha_inicio: moment(startDate).format('YYYY-MM'),
        fecha_final: moment(endDate).format('YYYY-MM'),
        id_estaciones: selectedOptions.toString(),
        columnas: selectedColumns.toString(),
        adicionales,
      },
    });
    const resps = await respuesta.data;
    return resps;
  };
  const [isLoading, setIsLoading] = useState(false);
  const [pro, setPro] = useState('-1');
  const [met, setMet] = useState('-1');
  const [metosel, setMetosel] = useState('0');
  const [startDate, setStartDate] = useState(new Date('1900/01/01'));
  const [endDate, setEndDate] = useState(new Date());
  const [est, setEst] = useState([]);
  const [col, setCol] = useState([]);
  const [filt, setFilt] = useState([]);
  const [dat, setDat] = useState(null);
  const [adi, setAdi] = useState({});
  const [selectedOptions, setSelectedOptions] = React.useState([]);
  const [selectedColumns, setSelectedColumns] = React.useState([]);
  const [column, setColumn] = React.useState([]);
  const { project } = useContext(AuthContext);

  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project).codigo.toString();
  }
  const [prosel, setProsel] = useState(proyecto);

  const BOOTSTRAP_CLASSES = {
    filter: 'form-control',
    select: 'form-control',
    button: 'btn btn btn-block btn-default',
    buttonActive: 'btn btn btn-block btn-primary',
  };

  const defCol = ['ID_ESTACION', 'NOM_ESTACION', 'ENTIDADES', 'FECHA_MES'];

  useEffect(() => {
    setIsLoading(true);
    request1().then((data1) => {
      setPro(data1);
    });
    if (proyecto) {
      request2(proyecto).then((data2) => {
        data2.sort((a, b) => {
          // console.log('esto es a', a, 'esto es b  ', b);
          if (a.data.nombre && b.data.nombre) {
            if (a.data.nombre.toLowerCase() > b.data.nombre.toLowerCase()) {
              return 1;
            }
            if (a.data.nombre.toLowerCase() < b.data.nombre.toLowerCase()) {
              return -1;
            }
            return 0;
          }
        });
        setMet(data2);
      });
    }
  }, []);

  const handleChange = (event) => {
    setIsLoading(true);
    setProsel(event.target.value);

    request2(event.target.value).then((data2) => {
      data2.sort((a, b) => {
        // console.log('esto es a', a, 'esto es b  ', b);
        if (a.data.nombre && b.data.nombre) {
          if (a.data.nombre.toLowerCase() > b.data.nombre.toLowerCase()) {
            return 1;
          }
          if (a.data.nombre.toLowerCase() < b.data.nombre.toLowerCase()) {
            return -1;
          }
          return 0;
        }
      });
      setMet(data2);
    });
  };

  const handleChangeMet = (event1) => {
    setMetosel(event1.target.value);
    setIsLoading(true);
    request3(prosel, event1.target.value, startDate, endDate).then((data3) => {
      setEst(data3.Estaciones);
      setCol(data3.Columnas);
      setFilt(data3.Columnas_filtros);

      const y = data3.Columnas.filter((value) => {
        if (defCol.includes(value.COLUMN_NAME)) {
          return value;
        }
      });

      setSelectedColumns(y);
      setSelectedOptions([]);
    });
  };

  const generarColumnas = (selectedColumns) => {
    const cols = [];
    let x;
    for (x in selectedColumns) {
      const buff = {};
      buff.Header = selectedColumns[x].COLUMN_NAME.toString();
      buff.accessor = selectedColumns[x].COLUMN_NAME;
      cols.push(buff);
    }
    cols.push({
      Header: 'No. REGISTROS',
      accessor: 'COUNT',
    });
    setColumn(cols);
  };

  const handleGenerar = (
    prosel,
    metsel,
    startDate,
    endDate,
    selectedOptions,
    selectedColumns,
    filt,
  ) => {
    const est = [];
    const col = [];
    let x;
    let adic = '';
    for (x in filt) {
      if (filt[x].VALUE) {
        adic += ' AND ';
        if (filt[x].TIPO_DATO == 3) {
          adic += ` REGEXP_LIKE (${filt[x].COLUMN_NAME}, '^.*${filt[x].VALUE}.*$', 'i')`;
        } else {
          adic += `${filt[x].COLUMN_NAME} = '${filt[x].VALUE}'`;
        }
      }
    }
    for (x in selectedOptions) {
      est.push(`'${selectedOptions[x].value.toString()}'`);
    }
    for (x in selectedColumns) {
      col.push(selectedColumns[x].COLUMN_NAME.toString());
    }
    request4(prosel, metsel, startDate, endDate, est, col, adic).then((data) => {
      data.Datos.forEach((value) => {
        value.FECHA = moment(value.FECHA).format('YYYY-MM-DD');
      });

      setDat(data.Datos);
      const adi1 = [];
      adi1.Nombre_proyecto = pro.find((pro) => {
        if (pro.codigo == prosel) {
          return pro.nombre_alterno;
        }
      }).nombre_alterno;
      adi1.Nombre_metodologia = met.find((met) => {
        if (met.id_metodologia == metsel) {
          return met.data.nombre;
        }
      }).data.nombre;
      adi1.Fconsulta = moment().format('YYYY/MM/DD');
      adi1.Total_registros = data.Total_registros;
      adi1.Count_estaciones = data.Count_estaciones;
      adi1.Total_tabla = data.Total_tabla;
      setAdi(adi1);
    });
    generarColumnas(selectedColumns);
  };

  const handleDeselect = (deselectedOptions) => {
    const selectedOptions1 = selectedOptions.slice();
    deselectedOptions.forEach((option) => {
      selectedOptions1.splice(selectedOptions1.indexOf(option), 1);
    });
    setSelectedOptions(selectedOptions1);
  };

  const handleSelect = (selectedOptions) => {
    selectedOptions.sort((a, b) => a.value - b.value);
    setSelectedOptions(selectedOptions);
  };

  const handleDeselect1 = (deselectedOptions) => {
    const selectedOptions1 = selectedColumns.slice();
    deselectedOptions.forEach((option) => {
      if (!(option in defCol)) {
        selectedOptions1.splice(selectedColumns.indexOf(option), 1);
      }
    });
    setSelectedColumns(selectedOptions1);
  };

  const handleSelect1 = (selectedOptions) => {
    setSelectedColumns(selectedOptions);
  };

  return (
    <div
      className="container"
      style={{
        marginBottom: '10px',
        display: 'grid',
      }}
    >
      <br />
      <h1>Control de Ingreso de datos</h1>
      <br />
      <form>
        <label
          style={{
            display: 'block',
          }}
        >
          Proyecto
        </label>
        <select value={prosel} onChange={handleChange} className="text-input">
          <option key="Nada" value="-1">
            Seleccione el Proyecto
          </option>
          {pro != '-1'
            ? pro.map((pro) => (
              <option key={pro.nombre_alterno} value={pro.codigo}>
                  {pro.nombre_alterno} - ID:{pro.codigo}
                </option>
              ))
            : []}
        </select>

        <br />

        <label
          style={{
            display: 'block',
          }}
        >
          Metodología
        </label>
        <select
          value={metosel}
          onChange={handleChangeMet}
          display={prosel > '0' ? 'block' : 'none'}
          className="text-input"
        >
          <option key="Nada" value="-1">
            Selecciona la Metodología
          </option>

          {met != '-1'
            ? met.map((met) => (
              <option key={met.id_metodologia} value={met.id_metodologia}>
                  {met.data.nombre} - ID: {met.id_metodologia}
                </option>
              ))
            : []}
        </select>
        <br />

        <div>
          <label
            style={{
              display: 'block',
            }}
          >
            Fecha Inicial de Consulta
          </label>

          <DatePicker
            selected={startDate}
            onChange={(date) => setStartDate(date)}
            showYearDropdown
            yearDropdownItemNumber={15}
            scrollableYearDropdown
            selectsStart
            startDate={startDate}
            endDate={endDate}
            dateFormat="MM/yyyy"
            showMonthYearPicker
            className="text-input"
          />

          <label
            style={{
              display: 'block',
            }}
          >
            Fecha Final de consulta
          </label>
          <DatePicker
            selected={endDate}
            onChange={(date) => setEndDate(date)}
            showMonthYearPicker
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            dateFormat="MM/yyyy"
            className="text-input"
          />
        </div>
        <br />
        <div>
          <label
            style={{
              display: 'block',
            }}
          >
            Estaciones Asociadas al proyecto: {est.length - selectedOptions.length}
          </label>
          <FilteredMultiSelect
            buttonText="Agregar"
            classNames={BOOTSTRAP_CLASSES}
            onChange={handleSelect}
            options={est}
            selectedOptions={selectedOptions}
            textProp="label"
            valueProp="value"
            size={5}
            placeholder="Filtrar Estaciones"
          />
        </div>

        <br />
        <div>
          <label
            style={{
              display: 'block',
            }}
          >
            Estaciones Seleccionadas: {selectedOptions.length}
          </label>
          <FilteredMultiSelect
            buttonText="Remover"
            classNames={{
              filter: 'form-control',
              select: 'form-control',
              button: 'btn btn btn-block btn-default',
              buttonActive: 'btn btn btn-block btn-danger',
            }}
            onChange={handleDeselect}
            options={selectedOptions}
            textProp="label"
            valueProp="value"
            size={5}
            placeholder="Filtrar Estaciones Seleccionadas"
          />
        </div>

        <br />
        <div>
          <div>
            <label
              style={{
                display: 'block',
              }}
            >
              Columnas Disponibles
            </label>
            <FilteredMultiSelect
              buttonText="Agregar"
              classNames={BOOTSTRAP_CLASSES}
              onChange={handleSelect1}
              options={col}
              selectedOptions={selectedColumns}
              textProp="COLUMN_NAME"
              valueProp="COLUMN_ID"
              size={5}
              placeholder="Filtrar columnas "
            />
          </div>

          <br />
          <div>
            <label
              style={{
                display: 'block',
              }}
            >
              Columnas Seleccionadas
            </label>
            <FilteredMultiSelect
              buttonText="Remover"
              classNames={{
                filter: 'form-control',
                select: 'form-control',
                button: 'btn btn btn-block btn-default',
                buttonActive: 'btn btn btn-block btn-danger',
              }}
              onChange={handleDeselect1}
              options={selectedColumns}
              textProp="COLUMN_NAME"
              valueProp="COLUMN_ID"
              size={5}
              placeholder="Filtrar columnas"
            />
          </div>
        </div>
        <br />

        <div>
          {selectedColumns === [] ? (
            <></>
          ) : (
            <label
              style={{
                display: 'block',
              }}
            >
              Filtros Adicionales
            </label>
          )}
          <div>
            {selectedColumns.map((value) => {
              let y;
              for (y in filt) {
                if (
                  value.COLUMN_NAME === 'ENTIDADES' &&
                  !filt.some((value1) => value1.COLUMN_NAME === 'ENTIDADES')
                ) {
                  filt.push({
                    COLUMN_NAME: 'ENTIDADES',
                    TIPO_DATO: 3,
                  });
                }
                if (
                  value.COLUMN_NAME === 'PARTICIPANTES' &&
                  !filt.some((value1) => value1.COLUMN_NAME === 'PARTICIPANTES')
                ) {
                  filt.push({
                    COLUMN_NAME: 'PARTICIPANTES',
                    TIPO_DATO: 3,
                  });
                }
                if (value.COLUMN_NAME === filt[y].COLUMN_NAME) {
                  switch (filt[y].TIPO_DATO) {
                    case 1:
                      return (
                        <div key={filt[y].COLUMN_NAME}>
                          {filt[y].COLUMN_NAME}
                          <input
                            key={filt[y].COLUMN_NAME}
                            type="Number"
                            onChange={(e) => {
                              filt[y].VALUE = e.target.value;
                            }}
                            placeholder="Ingrese el valor numerico"
                            className="text-input"
                          />
                        </div>
                      );

                    case 2:
                      break;

                    case 3:
                      return (
                        <div key={filt[y].COLUMN_NAME}>
                          {filt[y].COLUMN_NAME}
                          <input
                            key={filt[y].COLUMN_NAME}
                            type="Text"
                            onChange={(e) => {
                              filt[y].VALUE = e.target.value;
                            }}
                            placeholder="Ingrese el valor a filtrar"
                            className="text-input"
                          />
                        </div>
                      );
                    case 4:
                      return (
                        <div key={filt[y].COLUMN_NAME}>
                          {filt[y].COLUMN_NAME}
                          <select
                            key={filt[y].COLUMN_NAME}
                            onChange={(e) => {
                              filt[y].VALUE = e.target.value;
                            }}
                            placeholder="Seleccione una de las opciones"
                            className="text-input"
                          >
                            <option key="NADA" value="-1">
                              Sin Filtro
                            </option>
                            {filt[y].OPCIONES.map((value) => (
                              <option key={value.CODIGO} value={value.CODIGO}>
                                {value.CODIGO} - {value.DESCRIPCION}
                              </option>
                            ))}
                          </select>
                        </div>
                      );
                    default:
                      break;
                  }
                }
              }
            })}
          </div>
        </div>
      </form>
      <br />
      <button
        disabled={selectedOptions[0] == undefined}
        onClick={() =>
          handleGenerar(
            prosel,
            metosel,
            startDate,
            endDate,
            selectedOptions,
            selectedColumns,
            filt,
          )
        }
        className="btn btn btn-block btn-primary"
        style={{
          float: 'rigth',
          display: 'inline-block',
          padding: '4px 20px',
          fontSize: '17px',
        }}
      >
        Generar Tabla
      </button>
      <br />
      {dat == null ? (
        []
      ) : (
        <>
          <br />

          <Tabla columns={column} data={dat} adi={adi} />
          <button
            className="btn btn btn-block btn-primary"
            style={{
              float: 'rigth',
              display: 'inline-block',
              padding: '4px 20px',
              fontSize: '17px',
            }}
            onClick={() => {
              const est = [];
              const col = [];
              let x;

              for (x in selectedOptions) {
                est.push(`'${selectedOptions[x].value.toString()}'`);
              }
              for (x in selectedColumns) {
                col.push(selectedColumns[x].COLUMN_NAME.toString());
              }
              let adic = '';
              for (x in filt) {
                if (filt[x].VALUE) {
                  adic += ' AND ';
                  if (filt[x].TIPO_DATO == 3) {
                    adic += ` REGEXP_LIKE (${filt[x].COLUMN_NAME}, '^.*${filt[x].VALUE}.*$', 'i')`;
                  } else {
                    adic += `${filt[x].COLUMN_NAME} = '${filt[x].VALUE}'`;
                  }
                }
              }
              const buf1 = '';
              const nArchivo = buf1.concat(
                adi.Nombre_proyecto.toString(),
                '(',
                adi.Nombre_metodologia.toString(),
                ').xlsx',
              );
              // console.log(nArchivo);
              Excel(
                'post',
                'control/export/',
                {
                  nombre_proyecto: adi.Nombre_proyecto,
                  nombre_metodologia: adi.Nombre_metodologia,
                  id_proyecto: prosel,
                  id_metodologia: metosel,
                  fecha_inicio: moment(startDate).format('YYYY-MM'),
                  fecha_final: moment(endDate).format('YYYY-MM'),
                  id_estaciones: est.toString(),
                  columnas: col.toString(),
                  fconsulta: adi.Fconsulta,
                  total_registros: adi.Total_registros,
                  total_tabla: adi.Total_tabla,
                  adicionales: adic,
                  count_estaciones: adi.Count_estaciones,
                },
                nArchivo,
              );
            }}
          >
            Descargar
          </button>
        </>
      )}
    </div>
  );
};

export default CtrView;
