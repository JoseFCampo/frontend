import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const ParView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/parametros/`,
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_parametro}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [par, setPar] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setPar(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/parCreate');
  };

  if (par === null) {
    return (
      <div>
        <h1>Parámetros</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Parámetros',
      columns: [
        {
          Header: 'Identificador del Parámetro',
          accessor: 'id_parametro',
          width: 100,
        },
        {
          Header: 'Nombre del parámetro',
          accessor: 'nombre',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Código de letras',
          accessor: 'codigo_letras',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Siglas',
          accessor: 'siglas',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/parUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Parámetros</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={par && par ? par : []} columns={columns} />
      )}
    </div>
  );
};

export default ParView;
