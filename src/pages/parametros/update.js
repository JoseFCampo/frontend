import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const ParUpdate = () => {
  const { authAxios } = useContext(FetchContext);

  const request = async (id_parametro) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/parametros/u/${id_parametro}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [par, setPar] = useState(null);
  const [resp, setResp] = useState(null);
  const id = useLocation();
  const id_parametro = id.search.split('=')[1];
  useEffect(() => {
    request(id_parametro).then((data) => {
      setPar(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (par === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de parámetros ...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Parámetros</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            id_parametro: par.id_parametro,
            nombre: par.nombre,
            codigo_letras: par.codigo_letras,
            siglas: par.siglas,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/parametros/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().nullable(),
            codigo_letras: Yup.string().nullable(),
            siglas: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador del parámetro
                </label>
                <input
                  id="id_parametro"
                  type="Number"
                  title="No se puede modificar el identificador numerico del parámetro"
                  value={values.id_parametro}
                  readOnly
                  disabled
                  className="text-input"
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del parámetro
                </label>
                <input
                  id="nombre"
                  type="Text"
                  title="No se puede modificar el código numerico del parerente"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Código alfabetico
                </label>
                <input
                  id="codigo_letras"
                  placeholder="Ingrese el identificador Alfabetico"
                  type="Text"
                  value={values.codigo_letras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.codigo_letras && touched.codigo_letras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.codigo_letras && touched.codigo_letras && (
                  <div className="input-feedback">{errors.codigo_letras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Siglas
                </label>
                <input
                  id="siglas"
                  placeholder="Ingrese las siglas del parámetro"
                  type="Text"
                  value={values.siglas}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.siglas && touched.siglas
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.siglas && touched.siglas && (
                  <div className="input-feedback">{errors.siglas}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Parámetro
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default ParUpdate;
