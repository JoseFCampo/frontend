import React, { useState, useEffect, useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const ParCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Parámetros</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            nombre: '',
            codigo_letras: '',
            siglas: '',
          }}
          onSubmit={async (values) => {
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/parametros/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            codigo_letra: Yup.string().nullable(),
            siglas: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del parámetro
                </label>
                <input
                  id="nombre"
                  type="Text"
                  title="Ingrese el nombre del parámetro Maximo 100 caracteres"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Código alfabetico
                </label>
                <input
                  id="codigo_letras"
                  placeholder="Ingrese el identificador Alfabetico"
                  type="Text"
                  value={values.codigo_letras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.codigo_letras && touched.codigo_letras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.codigo_letras && touched.codigo_letras && (
                  <div className="input-feedback">{errors.codigo_letras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Siglas
                </label>
                <input
                  id="siglas"
                  placeholder="Ingrese las siglas del parámetro"
                  type="Text"
                  value={values.siglas}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.siglas && touched.siglas
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.siglas && touched.siglas && (
                  <div className="input-feedback">{errors.siglas}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Parámetro
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default ParCreate;
