import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const ProView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/proyectos/`,
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.codigo}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [par, setPar] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setPar(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/proCreate');
  };

  if (par === null) {
    return (
      <div>
        <h1>Proyectos</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
          <Spinner/>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Proyecto',
      columns: [
        {
          Header: 'Código del Proyecto',
          accessor: 'codigo',
          width: 100,
        },
        {
          Header: 'Nombre del Proyecto',
          accessor: 'nombre_alterno',
        },
        {
          Header: 'Titulo del Proyecto',
          accessor: 'titulo',
        },
        {
          Header: 'Fecha de Inicio',
          accessor: 'finicio',
        },
        {
          Header: 'Fecha de Finalizacion',
          accessor: 'ffinalizo',
        },
        {
          Header: 'Entidad Ejecutora',
          accessor: 'ejecutor',
        },
      ],
    },
  ];

  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/proUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Proyectos </h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={par && par ? par : []} columns={columns} />
      )}
    </div>
  );
};

export default ProView;
