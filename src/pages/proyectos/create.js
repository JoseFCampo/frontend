import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import DatePicker from '../../components/DatePicker';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const ProCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Proyectos</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            nombre: '',
            titulo: '',
            ejecutor: '',
          }}
          onSubmit={async (values) => {
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/proyectos/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            titulo: Yup.string().required('Campo Obligatorio').trim(),
            ejecutor: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del Proyecto
                </label>
                <input
                  id="nombre_alterno"
                  type="Text"
                  title="Ingrese el nombre alterno del Proyecto Maximo 100 caracteres"
                  value={values.nombre_alterno}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre_alterno && touched.nombre_alterno
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre_alterno && touched.nombre_alterno && (
                  <div className="input-feedback">{errors.nombre_alterno}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Titulo del proyecto
                </label>
                <textarea
                  id="titulo"
                  placeholder="Ingrese el identificador Alfabetico"
                  value={values.titulo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.titulo && touched.titulo
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.titulo && touched.titulo && (
                  <div className="input-feedback">{errors.titulo}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Entidad Ejecutora
                </label>
                <input
                  id="ejecutor"
                  placeholder="Ingrese la entidad ejecutora del proyecto"
                  type="Text"
                  value={values.ejecutor}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.ejecutor && touched.ejecutor
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.ejecutor && touched.ejecutor && (
                  <div className="input-feedback">{errors.ejecutor}</div>
                )}
                <br />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Fecha de Inicio
                </label>
                <DatePicker
                  name="finicio"
                  value={values.finicio}
                  onChange={setFieldValue}
                  onBlur={handleBlur}
                  className={
                    errors.finicio && touched.finicio
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.finicio && touched.finicio && (
                  <div className="input-feedback">{errors.finicio}</div>
                )}
                <br />
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Fecha de Finalizacion
                </label>
                <DatePicker
                  name="ffinal"
                  value={values.ffinal}
                  onChange={setFieldValue}
                  onBlur={handleBlur}
                  className={
                    errors.ffinal && touched.ffinal
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.ffinal && touched.ffinal && (
                  <div className="input-feedback">{errors.ffinal}</div>
                )}
                <br />
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Proyecto
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default ProCreate;
