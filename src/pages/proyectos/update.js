import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import DatePicker from '../../components/DatePicker';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const ProUpdate = () => {
  const { authAxios } = useContext(FetchContext);

  const request = async (codigo) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/proyectos/u/${codigo}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [pro, setPro] = useState(null);
  const [resp, setResp] = useState(null);
  const id = useLocation();
  const codigo = id.search.split('=')[1];
  useEffect(() => {
    request(codigo).then((data) => {
      setPro(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (pro === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Proyectos ...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Proyectos</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            codigo: pro.codigo,
            nombre_alterno: pro.nombre_alterno,
            titulo: pro.titulo,
            ejecutor: pro.ejecutor,
            finicio: pro.finicio,
            ffinalizo: pro.ffinalizo,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/Proyectos/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            codigo: Yup.number().required('Campo Obligatorio'),
            nombre_alterno: Yup.string().required('Campo Obligatorio'),
            titulo: Yup.string().nullable(),
            ejecutor: Yup.string().nullable(),
            finicio: Yup.string().nullable(),
            ffinalizo: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador del Proyecto
                </label>
                <input
                  id="codigo"
                  type="Number"
                  title="No se puede modificar el identificador numerico del Proyecto"
                  value={values.codigo}
                  readOnly
                  disabled
                  className="text-input"
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre_alterno del Proyecto
                </label>
                <input
                  id="nombre_alterno"
                  type="Text"
                  title="No se puede modificar el código numerico del proerente"
                  value={values.nombre_alterno}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre_alterno && touched.nombre_alterno
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre_alterno && touched.nombre_alterno && (
                  <div className="input-feedback">{errors.nombre_alterno}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Código alfabetico
                </label>
                <input
                  id="titulo"
                  placeholder="Ingrese el identificador Alfabetico"
                  type="Text"
                  value={values.titulo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.titulo && touched.titulo
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.titulo && touched.titulo && (
                  <div className="input-feedback">{errors.titulo}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Ejecutor
                </label>
                <input
                  id="ejecutor"
                  placeholder="Ingrese las ejecutor del Proyecto"
                  type="Text"
                  value={values.ejecutor}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.ejecutor && touched.ejecutor
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.ejecutor && touched.ejecutor && (
                  <div className="input-feedback">{errors.ejecutor}</div>
                )}
                <br />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Fecha de Inicio
                </label>
                <DatePicker
                  name="finicio"
                  value={values.finicio}
                  onChange={setFieldValue}
                  onBlur={handleBlur}
                  className={
                    errors.finicio && touched.finicio
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.finicio && touched.finicio && (
                  <div className="input-feedback">{errors.finicio}</div>
                )}
                <br />
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Fecha de Finalizacion
                </label>
                <DatePicker
                  name="ffinal"
                  value={values.ffinal}
                  onChange={setFieldValue}
                  onBlur={handleBlur}
                  className={
                    errors.ffinal && touched.ffinal
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.ffinal && touched.ffinal && (
                  <div className="input-feedback">{errors.ffinal}</div>
                )}
                <br />
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Proyecto
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default ProUpdate;
