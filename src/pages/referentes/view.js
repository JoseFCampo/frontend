import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const RefView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(`${process.env.REACT_APP_API_URL}/aglov/`, {
      crossdomain: true,
    });
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.tabla},${row.cod_numerico}`;
      return row;
    });
    return conID;
  };

  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [ref, setRef] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setRef(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/refCreate');
  };

  if (ref === null) {
    return (
      <div>
        <h1>Referentes</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Tabla',
      columns: [
        {
          Header: 'Tabla No.',
          accessor: 'tabla',
          width: 100,
        },
        {
          Header: 'Descripción de la Tabla',
          accessor: 'descripcion_tabla',
          Filter: SelectColumnFilter,
        },
      ],
    },
    {
      Header: 'Referente',
      Filter: Text,
      columns: [
        {
          Header: 'Código Numerico',
          accessor: 'cod_numerico',
          width: 100,
        },
        {
          Header: 'Código Alfabetico',
          accessor: 'cod_alfabetico',
          width: 200,
        },
        {
          Header: 'Descripción del referente',
          accessor: 'descripcion',
          width: 150,
        },
        {
          Header: 'Orden',
          accessor: 'orden',
          width: 70,
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/refUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Referentes</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={ref && ref ? ref : []} columns={columns} />
      )}
    </div>
  );
};

export default RefView;
