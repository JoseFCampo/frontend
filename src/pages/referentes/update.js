import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 

import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const RefUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (tabla, cod_numerico) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/aglov/ref/${tabla},${cod_numerico}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [ref, setRef] = useState(null);
  const [resp, setResp] = useState(null);

  const id = useLocation();

  const dtab = id.search.split('=')[1].split(',')[0];
  const dcod = id.search.split('=')[1].split(',')[1];
  useEffect(() => {
    request(dtab, dcod).then((data) => {
      setRef(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (ref === null) {
    return (
      <div>
        <p>Cargando formulario de actualización...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Referente</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            tabla: ref.tabla,
            cod_numerico: ref.cod_numerico,
            descripcion: ref.descripcion,
            descripcion_tabla: ref.descripcion_tabla,
            cod_alfabetico: ref.cod_alfabetico,
            orden: ref.orden,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/aglov/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            orden: Yup.number()
              .integer('El orden debe ser un numero entero')
              .positive('El orden debe ser Positivo'),
            descripcion: Yup.string().nullable(),
            descripcion_tabla: Yup.string().nullable(),
            cod_alfabetico: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  htmlFor="tabla"
                  style={{
                    display: 'block',
                  }}
                >
                  Numero de tabla
                </label>
                <input
                  id="tabla"
                  type="Number"
                  title="No se puede modificar el valor de la tabla"
                  value={values.tabla}
                  disabled
                  className="text-input"
                />

                <label
                  htmlFor="descr_tabla"
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la tabla
                </label>
                <textarea
                  id="descripcion_tabla"
                  name="descripcion_tabla"
                  placeholder="Ingrese la descripción de la tabla"
                  value={values.descripcion_tabla}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.descripcion_tabla && touched.descripcion_tabla
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion_tabla && touched.descripcion_tabla && (
                  <div className="input-feedback">{errors.descripcion_tabla}</div>
                )}

                <label
                  htmlFor="cod_numerico"
                  style={{
                    display: 'block',
                  }}
                >
                  Código Numerico
                </label>
                <input
                  id="cod_numerico"
                  type="Number"
                  title="No se puede modificar el código numerico del referente"
                  value={values.cod_numerico}
                  disabled
                  className={
                    errors.cod_numerico && touched.cod_numerico
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_numerico && touched.cod_numerico && (
                  <div className="input-feedback">{errors.cod_numerico}</div>
                )}

                <label
                  htmlFor="cod_alfab"
                  style={{
                    display: 'block',
                  }}
                >
                  Código Alfabetico
                </label>
                <input
                  id="cod_alfabetico"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  value={values.cod_alfabetico}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_alfabetico && touched.cod_alfabetico
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_alfabetico && touched.cod_alfabetico && (
                  <div className="input-feedback">{errors.cod_alfabetico}</div>
                )}

                <label
                  htmlFor="descripcion"
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del referente
                </label>
                <textarea
                  id="descripcion"
                  name="descripcion"
                  placeholder="Ingrese la descripción del referente"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="400"
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <label
                  htmlFor="orden"
                  style={{
                    display: 'block',
                  }}
                >
                  Orden
                </label>
                <input
                  id="orden"
                  placeholder="Ingrese el Orden del referente"
                  type="Number"
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}

                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Referente
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default RefUpdate;
