import React, { useState, useEffect, useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const RefCreate = () => {
  const { authAxios } = useContext(FetchContext);

  const request = async () => {
    const [resp, setResp] = useState(null);
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/aglov/last/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const [tab, setTab] = useState(null);
  const [resp, setResp] = useState(null);

  useEffect(() => {
    request().then((data) => {
      setTab(data);
      // console.log(tab);
    });
  }, []);

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Referentes</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            tabla: '',
            cod_numerico: '',
            descripcion: '',
            descripcion_tabla: '',
            cod_alfabetico: '',
            orden: '',
          }}
          onSubmit={async (values) => {
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/aglov/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);

            alert(JSON.stringify(values));
          }}
          validationSchema={Yup.object().shape({
            tabla: Yup.number()
              .positive('El identificador de la tabla debe ser Positivo')
              .integer('El identicador de la tabla debe ser un numero entero')
              .required('Campo Obligatorio'),
            cod_numerico: Yup.number()
              .integer('El identicador debe ser un numero entero')
              .positive('El identificador del referente debe ser Positivo')
              .required('Campo Obligatorio'),
            orden: Yup.number()
              .integer('El orden debe ser un numero entero')
              .positive('El orden debe ser Positivo'),
            descripcion: Yup.string().required('Campo Obligatorio'),
            descripcion_tabla: Yup.string().nullable(),
            cod_alfabetico: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  htmlFor="tabla"
                  style={{
                    display: 'block',
                  }}
                >
                  Numero de tabla
                </label>
                <input
                  id="tabla"
                  title="El numero de la tabla, este debe un entero positivo"
                  placeholder="Ingrese el numero de la tabla"
                  type="Number"
                  value={values.tabla}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.tabla && touched.tabla ? 'text-input error' : 'text-input'
                  }
                />
                {errors.tabla && touched.tabla && (
                  <div className="input-feedback">{errors.tabla}</div>
                )}

                <label
                  htmlFor="descr_tabla"
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la tabla
                </label>
                <textarea
                  id="descripcion_tabla"
                  name="descripcion_tabla"
                  title="Describa la tabla. Maximo 100 Caracteres"
                  placeholder="Ingrese la descripción de la tabla"
                  value={values.descripcion_tabla}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.descripcion_tabla && touched.descripcion_tabla
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion_tabla && touched.descripcion_tabla && (
                  <div className="input-feedback">{errors.descripcion_tabla}</div>
                )}

                <label
                  htmlFor="cod_numerico"
                  style={{
                    display: 'block',
                  }}
                >
                  Código Numerico
                </label>
                <input
                  id="cod_numerico"
                  placeholder="Ingrese el Código Numerico"
                  title="Este es el identificador numerico del referente, debe ser un entero positivo"
                  type="Number"
                  value={values.cod_numerico}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_numerico && touched.cod_numerico
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_numerico && touched.cod_numerico && (
                  <div className="input-feedback">{errors.cod_numerico}</div>
                )}

                <label
                  htmlFor="cod_alfab"
                  style={{
                    display: 'block',
                  }}
                >
                  Código Alfabetico
                </label>
                <input
                  id="cod_alfabetico"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  title="Este es el identificador Alfabetico del referente. Maximo 10 Caracteres"
                  value={values.cod_alfabetico}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_alfabetico && touched.cod_alfabetico
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_alfabetico && touched.cod_alfabetico && (
                  <div className="input-feedback">{errors.cod_alfabetico}</div>
                )}

                <label
                  htmlFor="descripcion"
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del referente
                </label>
                <textarea
                  id="descripcion"
                  name="descripcion"
                  placeholder="Ingrese la descripción del referente"
                  title="Describa el referente. Longitud maxima: 400 Caracteres"
                  value={values.Descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="400"
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <label
                  htmlFor="orden"
                  style={{
                    display: 'block',
                  }}
                >
                  Orden
                </label>
                <input
                  id="orden"
                  placeholder="Ingrese el Orden del referente"
                  type="Number"
                  title="Describa el orden del referente dentro de la tabla si los referentes se encuentran ordenados. Este debe ser un entero positivo."
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Referente
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default RefCreate;
