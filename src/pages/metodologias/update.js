import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import DatePicker from '../../components/DatePicker';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const MtdUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (id_metodologia) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/metodologias/u/${id_metodologia}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [mtd, setMtd] = useState(null);
  const [resp, setResp] = useState(null);
  const id = useLocation();
  const id_metodologia = id.search.split('=')[1];
  useEffect(() => {
    request(id_metodologia).then((data) => {
      setMtd(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (mtd === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Metodologías...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Metodologías</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            id_metodologia: mtd.id_metodologia,
            nombre: mtd.nombre,
            version_plantilla: mtd.version_plantilla,
            descripcion: mtd.descripcion,
            des_muestras: mtd.des_muestras,
            fvigente_desde: mtd.fvigente_desde,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/metodologias/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            version_plantilla: Yup.string().nullable().trim(),
            descripcion: Yup.string().nullable(),
            des_muestras: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
              setFieldValue,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre de la Metodología
                </label>
                <input
                  id="nombre"
                  type="Text"
                  title="Ingrese el nombre de la metodología. Max 100 caracteres "
                  placeholder="Ingrese el nombre de la metodología"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la metodología
                </label>
                <textarea
                  id="descripcion"
                  placeholder="Ingrese la descripción de la metodología"
                  title="En este campo va la descripción del metodología, Max: 1000 Caracteres. "
                  type="Text"
                  maxLength="1000"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de las Muestras
                </label>
                <textarea
                  id="des_muestras"
                  placeholder="Ingrese la descripción de las muestras "
                  title="En este campo va la descripción muestras pertenecientes a esta metodología, Max: 3000 Caracteres. "
                  type="Text"
                  maxLength="3000"
                  value={values.des_muestras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.des_muestras && touched.des_muestras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.des_muestras && touched.des_muestras && (
                  <div className="input-feedback">{errors.des_muestras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Version de la plantilla para la metodología
                </label>
                <input
                  id="version_plantilla"
                  type="Text"
                  title="Ingrese la version de la plantilla del metodología Max 10 caracteres "
                  placeholder="Ingrese la version de la plantilla de la metodología"
                  value={values.version_plantilla}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.version_plantilla && touched.version_plantilla
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.version_plantilla && touched.version_plantilla && (
                  <div className="input-feedback">{errors.version_plantilla}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Fecha desde la que es Vigente la Metodología
                </label>
                <DatePicker
                  name="fvigente_desde"
                  value={values.fvigente_desde}
                  onChange={setFieldValue}
                  onBlur={handleBlur}
                  className={
                    errors.fvigente_desde && touched.fvigente_desde
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.fvigente_desde && touched.fvigente_desde && (
                  <div className="input-feedback">{errors.fvigente_desde}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Metodología
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default MtdUpdate;
