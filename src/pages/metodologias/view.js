import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const MtdView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/metodologias/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_metodologia}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [mtd, setMet] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setMet(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/mtdCreate');
  };

  if (mtd === null) {
    return (
      <div>
        <h1>Metodologías</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Metodologías',
      columns: [
        {
          Header: 'Id',
          accessor: 'id_metodologia',
          width: 100,
        },
        {
          Header: 'Nombre',
          accessor: 'nombre',
        },
        {
          Header: 'Descripción',
          accessor: 'descripcion',
        },
        {
          Header: 'Descripción Muestras',
          accessor: 'des_muestras',
        },
        {
          Header: 'Version Plantilla',
          accessor: 'version_plantilla',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Vigente desde',
          accessor: 'fvigente_desde',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Fecha Sistema',
          accessor: 'fsistema',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/mtdUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Metodologías</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={mtd && mtd ? mtd : []} columns={columns} />
      )}
    </div>
  );
};

export default MtdView;
