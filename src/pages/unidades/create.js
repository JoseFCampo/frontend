import React, { useState, useEffect ,useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory} from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const UniCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de unidades</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            detalle: '',
            descripcion: '',
          }}
          onSubmit={async (values) => {
            //console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/unidades/create/`,
              data: values,
            });
            setResp(respuesta);
            //console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            detalle: Yup.string().required('Campo Obligatorio').trim(),
            descripcion: Yup.string().nullable().trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Detalle de la unidad de medida
                </label>
                <input
                  id="detalle"
                  type="Text"
                  title="Ingrese los detalles de la unidad de medida, por ejemplo el nombre"
                  placeholder="Ingrese los detalles de la unidad"
                  value={values.detalle}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.detalle && touched.detalle
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.detalle && touched.detalle && (
                  <div className="input-feedback">{errors.detalle}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la unidad de medida
                </label>
                <textarea
                  id="descripcion"
                  placeholder="Ingrese la descripción de la variable"
                  title="En este campo va la descripción de la unidad de medida,Longitud maxima: 100 Caracteres. "
                  type="Text"
                  maxLength="100"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Unidad
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default UniCreate;
