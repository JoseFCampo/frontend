import React, { useState, useEffect, useContext } from 'react';
 
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const UniView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/unidades/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_unidad_medida}`;
      return row;
    });
    return conID;
  };
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [ref, setRef] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setRef(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/uniCreate');
  };

  if (ref === null) {
    return (
      <div>
        <h1>Unidades de Medida</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Unidades',
      columns: [
        {
          Header: 'Identificador de la unidad',
          accessor: 'id_unidad_medida',
          width: 100,
        },
        {
          Header: 'Detalle de la unidad',
          accessor: 'detalle',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Descripción',
          accessor: 'descripcion',
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/uniUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Unidades</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

{isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={ref && ref ? ref : []} columns={columns} />
      )}
    </div>
  );
};

export default UniView;
