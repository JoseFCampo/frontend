import React, { useState, useEffect,useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const UniUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (id_unidad_medida) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/unidades/u/${id_unidad_medida}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [uni, setUni] = useState(null);
  const [resp, setResp] = useState(null);
  const id = useLocation();
  const id_unidad_medida = id.search.split('=')[1];
  useEffect(() => {
    request(id_unidad_medida).then((data) => {
      setUni(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (uni === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Unidades de Medida...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Unidades de Medida</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            id_unidad_medida: uni.id_unidad_medida,
            detalle: uni.detalle,
            descripcion: uni.descripcion,
          }}
          onSubmit={async (values) => {
            //console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/unidades/update/`,
              data: values,
            });
            setResp(respuesta);
            //console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            detalle: Yup.string().required('Campo Obligatorio').trim(),
            descripcion: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador de la unidad de medida
                </label>
                <input
                  id="id_unidad_medida"
                  type="Number"
                  title="No se puede modificar el valor de la unidad de medida"
                  value={values.id_unidad_medida}
                  readOnly
                  disabled
                  className="text-input"
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Detalle de la unidad de medida
                </label>
                <input
                  id="detalle"
                  type="Text"
                  title="No se puede modificar el código numerico del referente"
                  value={values.detalle}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.detalle && touched.detalle
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.detalle && touched.detalle && (
                  <div className="input-feedback">{errors.detalle}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la unidad de medida
                </label>
                <input
                  id="descripcion"
                  placeholder="Ingrese la descripción de la variable"
                  type="Text"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Unidad
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default UniUpdate;
