import React, { useState, useEffect, useContext } from 'react';
import Axios from 'axios';
import { useHistory, Link, useLocation } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

import Excel from '../../components/Export_excel';
import Table, { SelectColumnFilter } from '../../components/Table';
import '../../static/styles/forms.scss';

import { Spinner } from 'react-bootstrap';

const EspView = () => {
  const request = async (prosel, metsel) => {
    const response = await Axios.get(
      `${process.env.REACT_APP_API_URL}/especies/filt/${prosel},${metsel}/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.cod_letras},${row.id_proyecto},${row.id_metodologia}`;
      return row;
    });
    return conID;
  };
  const request1 = async () => {
    const proy = await Axios.get(`${process.env.REACT_APP_API_URL}/especies/proy/`, {
      crossdomain: true,
    });
    const resps = await proy.data;
    return resps;
  };
  const request2 = async (id_proyecto) => {
    const proy = await Axios.get(
      `${process.env.REACT_APP_API_URL}/especies/metd/${id_proyecto}/`,
      {
        crossdomain: true,
      },
    );
    const resps = await proy.data;
    return resps;
  };

  const history = useHistory();
  const { project, isAdmin } = useContext(AuthContext);
  let proyecto = 0;
  if (project) {
    proyecto = JSON.parse(project).codigo;
  }
  const [prosel, setProsel] = useState('0');
  const [isLoading, setIsLoading] = useState(true);
  const [esp, setEsp] = useState(null);
  const [pro, setPro] = useState('-1');
  const [met, setMet] = useState('-1');
  const [metsel, setMetsel] = useState('0');
  useEffect(() => {
    request1().then((data1) => {
      setPro(data1);
      if (data1.some((proys) => proys.codigo == proyecto)) {
        setProsel(proyecto);
        request2(proyecto).then((data2) => {
          setMet(data2);
        });
      } else {
        request2(prosel).then((data2) => {
          setMet(data2);
        });
      }
    });
  }, []);

  const handleChange = (event) => {
    setIsLoading(true);
    setProsel(event.target.value);
    setMetsel(0);
    setMet('-1');
    request2(event.target.value).then((data2) => {
      setMet(data2);
    });
  };

  const handleChangeMet = (event) => {
    setMetsel(event.target.value);
    setIsLoading(true);
  };

  const handleCrear = () => {
    history.push('/espCreate');
  };

  const handleGenerar = (prosel, metsel) => {
    setIsLoading(true);
    request(prosel, metsel).then((data) => {
      setEsp(data);
      setIsLoading(false);
    });
  };

  const columns = [
    {
      Header: 'Proyecto',
      columns: [
        {
          Header: 'Id Proyecto',
          accessor: 'id_proyecto',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Id Metodología',
          accessor: 'id_metodologia',
          Filter: SelectColumnFilter,
        },
      ],
    },
    {
      Header: 'Especie',
      columns: [
        {
          Header: 'Descripción de la Especie',
          accessor: 'descripcion',
        },
        {
          Header: 'Código Alfabetico',
          accessor: 'cod_letras',
        },
        {
          Header: 'Clave SIBM',
          accessor: 'clave_sibm',
        },
        {
          Header: 'Phylum',
          accessor: 'phylum',
        },
        {
          Header: 'Clase',
          accessor: 'clase',
        },
        {
          Header: 'Orden',
          accessor: 'orden',
        },
        {
          Header: 'Familia',
          accessor: 'familia',
        },
        {
          Header: 'Genero',
          accessor: 'genero',
        },
        {
          Header: 'Notas',
          accessor: 'notas',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/espUpdate?id=${row.value}`}> ✏ </Link>
              {/* <button onClick={() => handleEdit(row.value)}>Edit</button> */}
            </div>
          ),
        },
      ],
    });
  }
  return (
    <div>
      <br />
      <h1>Listado de especies</h1>
      <br />
      <select
        value={prosel}
        onChange={handleChange}
        style={{
          fontWeight: '700',
          height: '30px',
          maxWidth: '500px',
          fontSize: '15px',
          width: '30%',
          margin: '5px',
        }}
      >
        <option key="todas" value="0">
          Todos los Proyectos
        </option>

        {pro != '-1'
          ? pro.map((pro) => (
              <option key={pro.codigo} value={pro.codigo}>
              {pro.nombre_alterno}
            </option>
            ))
          : []}
      </select>
      <select
        value={metsel}
        onChange={handleChangeMet}
        // hidden={!(prosel != '0' && met != '-1')}
        style={{
          fontWeight: '700',
          height: '30px',
          maxWidth: '500px',
          fontSize: '15px',
          width: '30%',
          margin: '5px',
        }}
      >
        <option key="todas" value="0">
          Todas las Metodologías
        </option>

        {prosel == '1480' ? (
          <option key="802" value="802">
            802
          </option>
        ) : (
          []
        )}
        {met != '-1'
          ? met.map((met) => (
              <option key={met.id_metodologia} value={met.id_metodologia}>
              {met.nombre} - {met.id_metodologia}
            </option>
            ))
          : []}
      </select>
      <button
        onClick={() => handleGenerar(prosel, metsel)}
        style={{
          float: 'rigth',
          display: 'inline-block',
          padding: '4px 20px',
          fontSize: '17px',
        }}
      >
        Generar Tabla
      </button>
      <br />

      {isLoading ? 
        <Spinner/>
       : (
        <div>
          <br />
          {isAdmin() ? (
            <button
              onClick={() => handleCrear()}
              style={{
                float: 'rigth',
                display: 'inline-block',
                marginLeft: '2px',
              }}
            >
              Crear
            </button>
          ) : (
            []
          )}
          <button
            onClick={() => {
              const data = {
                id_proyecto: prosel,
                id_metodologia: metsel,
              };

              Excel('post', 'especies/excel/', data, 'Lista de Especies.xlsx');
            }}
            style={{
              float: 'rigth',
              display: 'inline-block',
            }}
          >
            Descargar
          </button>
          <Table data={esp && esp ? esp : []} columns={columns} />
        </div>
      )}
    </div>
  );
};

export default EspView;
