import React, { useState, useEffect, useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Message from '../../components/Message';
import Table from '../../components/TablaWorms';
import { FetchContext } from '../../context/FetchContext';

const EspCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const requestW = async (name) => {
    const worms = await authAxios.get(
      `https://www.marinespecies.org/rest/AphiaRecordsByName/${name}?like=true&marine_only=true&offset=1`,
      {
        crossdomain: true,
      },
    );
    const resps = await worms.data;
    return resps;
  };

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const handleWorms = (name) => {
    requestW(name).then((data) => {
      setRespW(data);
    });
  };
  const [mes, setMes] = useState(null);
  const [resp, setResp] = useState(null);
  const [respW, setRespW] = useState(null);

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Especie</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            cod_letras: '',
            id_proyecto: '',
            id_metodologia: '',
            descripcion: '',
            clave_sibm: '',
            phylum: '',
            clase: '',
            orden: '',
            familia: '',
            genero: '',
            notas: '',
          }}
          onSubmit={async (values) => {
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/especies/create/`,
              data: values,
            });
            setResp(respuesta);
          }}
          onWorms
          validationSchema={Yup.object().shape({
            cod_letras: Yup.string().required('Campo Obligatorio'),
            id_proyecto: Yup.number()
              .positive('El identificador de la tabla debe ser Positivo')
              .integer('El identicador de la tabla debe ser un numero entero')
              .required('Campo Obligatorio'),
            id_metodologia: Yup.number()
              .positive('El identificador de la tabla debe ser Positivo')
              .integer('El identicador de la tabla debe ser un numero entero')
              .required('Campo Obligatorio'),
            descripcion: Yup.string(),
            clave_sibm: Yup.string(),
            phylum: Yup.string().nullable(),
            clase: Yup.string().nullable(),
            orden: Yup.string().nullable(),
            familia: Yup.string().nullable(),
            genero: Yup.string().nullable(),
            notas: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;

            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación del Proyecto
                </label>
                <input
                  id="id_proyecto"
                  title="Este es el código de identificación del proyecto donde se registrara la especie, este debe un entero positivo"
                  placeholder="Ingrese el id del Proyecto"
                  type="Number"
                  value={values.id_proyecto}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.id_proyecto && touched.id_proyecto
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.id_proyecto && touched.id_proyecto && (
                  <div className="input-feedback">{errors.id_proyecto}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación de la metodología
                </label>
                <input
                  id="id_metodologia"
                  title="Este es el código de identificación de la metodología donde se registrara la especie, este debe un entero positivo"
                  placeholder="Ingrese el id de la metodología"
                  type="Number"
                  value={values.id_metodologia}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.id_metodologia && touched.id_metodologia
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.id_metodologia && touched.id_metodologia && (
                  <div className="input-feedback">{errors.id_metodologia}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación Alfabetica de la Especie
                </label>
                <input
                  id="cod_letras"
                  title="Este es el código de identificación alfabetica de la especie, esta es una abreviacion del nombre de la especie"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  value={values.cod_letras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_letras && touched.cod_letras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_letras && touched.cod_letras && (
                  <div className="input-feedback">{errors.cod_letras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación SIBM de la Especie
                </label>
                <input
                  id="clave_sibm"
                  title="Este es el código de identificación SIBM de la especie,"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  value={values.clave_sibm}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.clave_sibm && touched.clave_sibm
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.clave_sibm && touched.clave_sibm && (
                  <div className="input-feedback">{errors.clave_sibm}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre de la especie
                </label>
                <input
                  id="descripcion"
                  name="descripcion"
                  title="Describa la espcie. Maximo 100 Caracteres"
                  placeholder="Ingrese la descripción de la Especie"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}
                <br />
                <div>
                  <button
                    type="button"
                    onClick={() => {
                      handleWorms(values.descripcion);
                    }}
                    disabled={isSubmitting}
                  >
                    Buscar en Worms
                  </button>
                  {respW ? <Table data={respW && respW ? respW : []} /> : []}
                </div>
                <br />
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Pylum
                </label>
                <textarea
                  id="phylum"
                  name="phylum"
                  title="Ingrese el phylum de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el phylum de la Especie"
                  value={values.phylum}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.phylum && touched.phylum
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.phylum && touched.phylum && (
                  <div className="input-feedback">{errors.phylum}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Clase
                </label>
                <textarea
                  id="clase"
                  name="clase"
                  title="Ingrese la clase de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese la clase de la Especie"
                  value={values.clase}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.clase && touched.clase ? 'text-input error' : 'text-input'
                  }
                />
                {errors.clase && touched.clase && (
                  <div className="input-feedback">{errors.clase}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Orden
                </label>
                <textarea
                  id="orden"
                  name="orden"
                  title="Ingrese el orden de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el orden de la Especie"
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Genero
                </label>
                <textarea
                  id="genero"
                  name="genero"
                  title="Ingrese el genero de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el genero de la Especie"
                  value={values.genero}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.genero && touched.genero
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.genero && touched.genero && (
                  <div className="input-feedback">{errors.genero}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Familia de la especie
                </label>
                <textarea
                  id="familia"
                  name="familia"
                  title="Ingrese la familia de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese la familia de la Especie"
                  value={values.familia}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.familia && touched.familia
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.familia && touched.familia && (
                  <div className="input-feedback">{errors.familia}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Notas
                </label>
                <textarea
                  id="notas"
                  name="notas"
                  title="Ingrese la notas sobre la especie. Maximo 500 Caracteres"
                  placeholder="Ingrese la notas de la Especie"
                  value={values.notas}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="500"
                  className={
                    errors.notas && touched.notas ? 'text-input error' : 'text-input'
                  }
                />
                {errors.notas && touched.notas && (
                  <div className="input-feedback">{errors.notas}</div>
                )}

                <br />

                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Especie
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default EspCreate;
