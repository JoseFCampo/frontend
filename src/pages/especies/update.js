import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import Table from '../../components/TablaWorms';
import { FetchContext } from '../../context/FetchContext';

const EspUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const requestW = async (name) => {
    const worms = await authAxios.get(
      `https://www.marinespecies.org/rest/AphiaRecordsByName/${name}?like=true&marine_only=true&offset=1`,
      {
        crossdomain: true,
      },
    );
    const resps = await worms.data;
    return resps;
  };
  const request = async (cod_letras, id_proyecto, id_metodologia) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/especies/u/${id_proyecto},${cod_letras},${id_metodologia}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [ref, setRef] = useState(null);
  const [resp, setResp] = useState(null);
  const [respW, setRespW] = useState(null);
  const id = useLocation();

  const codl = id.search.split('=')[1].split(',')[0];
  const idp = id.search.split('=')[1].split(',')[1];
  const idm = id.search.split('=')[1].split(',')[2];
  useEffect(() => {
    request(codl, idp, idm).then((data) => {
      setRef(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  const handleWorms = (name) => {
    requestW(name).then((data) => {
      setRespW(data);
    });
  };

  if (ref === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de especie...</p>
      </div>
    );
  }

  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Especies</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            cod_letras: ref.cod_letras,
            id_proyecto: ref.id_proyecto,
            id_metodologia: ref.id_metodologia,
            descripcion: ref.descripcion,
            clave_sibm: ref.clave_sibm,
            phylum: ref.phylum,
            clase: ref.clase,
            orden: ref.orden,
            familia: ref.familia,
            genero: ref.genero,
            notas: ref.notas,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/aglov/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            clave_sibm: Yup.string(),
            descripcion: Yup.string(),
            phylum: Yup.string().nullable(),
            clase: Yup.string().nullable(),
            orden: Yup.string().nullable(),
            familia: Yup.string().nullable(),
            genero: Yup.string().nullable(),
            notas: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación del Proyecto
                </label>
                <input
                  id="id_proyecto"
                  title="No se puede modificar el código de identificación del proyecto donde se registro la especie, este es un indentificador dentro de la base de datos."
                  type="Number"
                  value={values.id_proyecto}
                  disabled
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación de la metodología
                </label>
                <input
                  id="id_metodologia"
                  title="No se puede alterar el código de identificación de la metodología donde se registro la especie, este es un indentificador dentro de la base de datos."
                  type="Number"
                  value={values.id_metodologia}
                  disabled
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción de la especie
                </label>
                <textarea
                  id="descripcion"
                  name="descripcion"
                  title="Describa la espcie. Maximo 100 Caracteres"
                  placeholder="Ingrese la descripción de la Especie"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <br />
                <div>
                  <button
                    type="button"
                    onClick={() => {
                      handleWorms(values.descripcion);
                    }}
                    disabled={isSubmitting}
                  >
                    Buscar en Worms
                  </button>
                  {respW ? <Table data={respW && respW ? respW : []} /> : []}
                </div>
                <br />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación Alfabetica de la Especie
                </label>
                <input
                  id="cod_letras"
                  title="No se puede modificar la identificación alfabetica de la especie, este es una indentificador dentro de la base de datos"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  value={values.cod_letras}
                  disabled
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificación SIBM de la Especie
                </label>
                <input
                  id="clave_sibm"
                  title="Este es el código de identificación SIBM de la especie,"
                  placeholder="Ingrese el identificador alfabetico"
                  type="Text"
                  value={values.clave_sibm}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.clave_sibm && touched.clave_sibm
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.clave_sibm && touched.clave_sibm && (
                  <div className="input-feedback">{errors.clave_sibm}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Pylum
                </label>
                <textarea
                  id="phylum"
                  name="phylum"
                  title="Ingrese el phylum de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el phylum de la Especie"
                  value={values.phylum}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.phylum && touched.phylum
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.phylum && touched.phylum && (
                  <div className="input-feedback">{errors.phylum}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Clase
                </label>
                <textarea
                  id="clase"
                  name="clase"
                  title="Ingrese la clase de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese la clase de la Especie"
                  value={values.clase}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.clase && touched.clase ? 'text-input error' : 'text-input'
                  }
                />
                {errors.clase && touched.clase && (
                  <div className="input-feedback">{errors.clase}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Orden
                </label>
                <textarea
                  id="orden"
                  name="orden"
                  title="Ingrese el orden de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el orden de la Especie"
                  value={values.orden}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.orden && touched.orden ? 'text-input error' : 'text-input'
                  }
                />
                {errors.orden && touched.orden && (
                  <div className="input-feedback">{errors.orden}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Genero
                </label>
                <textarea
                  id="genero"
                  name="genero"
                  title="Ingrese el genero de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese el genero de la Especie"
                  value={values.genero}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.genero && touched.genero
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.genero && touched.genero && (
                  <div className="input-feedback">{errors.genero}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Familia de la especie
                </label>
                <textarea
                  id="familia"
                  name="familia"
                  title="Ingrese la familia de la especie. Maximo 100 Caracteres"
                  placeholder="Ingrese la familia de la Especie"
                  value={values.familia}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="100"
                  className={
                    errors.familia && touched.familia
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.familia && touched.familia && (
                  <div className="input-feedback">{errors.familia}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Notas
                </label>
                <textarea
                  id="notas"
                  name="notas"
                  title="Ingrese la notas sobre la especie. Maximo 500 Caracteres"
                  placeholder="Ingrese la notas de la Especie"
                  value={values.notas}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  maxLength="500"
                  className={
                    errors.notas && touched.notas ? 'text-input error' : 'text-input'
                  }
                />
                {errors.notas && touched.notas && (
                  <div className="input-feedback">{errors.notas}</div>
                )}
                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar Especie
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default EspUpdate;
