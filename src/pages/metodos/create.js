import React, { useState, useEffect, useContext } from 'react';
import '../../static/styles/forms.scss';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const MetCreate = () => {
  const { authAxios } = useContext(FetchContext);
  const [resp, setResp] = useState(null);
  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <div>
      <div>
        <div className="crear">
          <h1>Formulario Creación de Métodos</h1>

          <br />
          <div>
            <button
              onClick={handleBack}
              style={{
                display: 'block',
              }}
            >
              Regresar{' '}
            </button>
            {resp ? <Message response={resp} /> : ''}
          </div>
          <br />
        </div>
        <Formik
          initialValues={{
            nombre: '',
            descripcion: '',
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'post',
              url: `${process.env.REACT_APP_API_URL}/metodos/create/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            descripcion: Yup.string().nullable().trim(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del Método
                </label>
                <input
                  id="nombre"
                  type="Text"
                  title="Ingrese el nombre del método. Max 100 caracteres "
                  placeholder="Ingrese el nombre del método"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador alfabetico del Método
                </label>
                <input
                  id="cod_letras"
                  type="Text"
                  title="Ingrese el identificador alfabetico del método Max 10 caracteres "
                  placeholder="Ingrese el identificador alfabetico del método"
                  value={values.cod_letras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_letras && touched.cod_letras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_letras && touched.cod_letras && (
                  <div className="input-feedback">{errors.cod_letras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del Método
                </label>
                <textarea
                  id="descripcion"
                  placeholder="Ingrese la descripción del método"
                  title="En este campo va la descripción del método, Max: 1000 Caracteres. "
                  type="Text"
                  maxLength="1000"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Crear Método
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default MetCreate;
