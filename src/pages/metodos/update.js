import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
 
import '../../static/styles/forms.scss';
import Message from '../../components/Message';
import { FetchContext } from '../../context/FetchContext';

const MetUpdate = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async (id_metodo) => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/metodos/u/${id_metodo}/`,
      {
        crossdomain: true,
      },
    );
    return response;
  };

  const [met, setMet] = useState(null);
  const [resp, setResp] = useState(null);

  const id = useLocation();
  const id_metodo = id.search.split('=')[1];
  useEffect(() => {
    request(id_metodo).then((data) => {
      setMet(data.data);
    });
  }, []);

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };
  if (met === null) {
    return (
      <div>
        <p>Cargando formulario de actualización de Metodo...</p>
      </div>
    );
  }
  return (
    <div>
      <div>
        <h1>Formulario de Modificación de Metodo</h1>
        <br />
        <div className="update">
          <button
            onClick={handleBack}
            style={{
              display: 'block',
            }}
          >
            Regresar{' '}
          </button>
          {resp ? <Message response={resp} /> : ''}
        </div>
        <br />
        <Formik
          initialValues={{
            id_metodo: met.id_metodo,
            nombre: met.nombre,
            cod_letras: met.cod_letras,
            descripcion: met.descripcion,
          }}
          onSubmit={async (values) => {
            // console.log(values);
            const respuesta = await authAxios({
              method: 'put',
              url: `${process.env.REACT_APP_API_URL}/metodos/update/`,
              data: values,
            });
            setResp(respuesta);
            // console.log(respuesta);
          }}
          validationSchema={Yup.object().shape({
            nombre: Yup.string().required('Campo Obligatorio').trim(),
            cod_letras: Yup.string().nullable().trim(),
            descripcion: Yup.string().nullable(),
          })}
        >
          {(props) => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset,
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador del método
                </label>
                <input
                  id="id_metodo"
                  type="Number"
                  title="No se puede modificar el identificador del método"
                  value={values.id_metodo}
                  readOnly
                  disabled
                  className="text-input"
                />

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Nombre del método
                </label>
                <input
                  id="nombre"
                  type="Text"
                  title="Ingrese el nombre del método Max 20 caracteres"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.nombre && touched.nombre
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.nombre && touched.nombre && (
                  <div className="input-feedback">{errors.nombre}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Identificador alfabetico del método
                </label>
                <input
                  id="cod_letras"
                  type="Text"
                  title="Identificador Alfabetico del método, Utilice Mayusculas"
                  value={values.cod_letras}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.cod_letras && touched.cod_letras
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.cod_letras && touched.cod_letras && (
                  <div className="input-feedback">{errors.cod_letras}</div>
                )}

                <label
                  style={{
                    display: 'block',
                  }}
                >
                  Descripción del método
                </label>
                <textarea
                  id="descripcion"
                  placeholder="Ingrese la descripción del método "
                  title="Ingrese la descripción del método, maximo 500 caracteres"
                  type="Text"
                  value={values.descripcion}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.descripcion && touched.descripcion
                      ? 'text-input error'
                      : 'text-input'
                  }
                />
                {errors.descripcion && touched.descripcion && (
                  <div className="input-feedback">{errors.descripcion}</div>
                )}

                <br />
                <button
                  type="button"
                  className="outline"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Limpiar Formulario
                </button>
                <button type="submit" disabled={isSubmitting}>
                  Actualizar método
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};
export default MetUpdate;
