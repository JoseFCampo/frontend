import React, { useState, useEffect, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Table, { SelectColumnFilter } from '../../components/Table';
import { AuthContext } from '../../context/AuthContext';
import { FetchContext } from '../../context/FetchContext';

const MetView = () => {
  const { authAxios } = useContext(FetchContext);
  const request = async () => {
    const response = await authAxios.get(
      `${process.env.REACT_APP_API_URL}/metodos/`,
      {
        crossdomain: true,
      },
    );
    const data = await response.data;
    const conID = data.map((row) => {
      row.editar = `${row.id_metodo}`;
      return row;
    });
    return conID;
  };

  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [met, setMet] = useState(null);
  const { project, isAdmin } = useContext(AuthContext);
  useEffect(() => {
    setIsLoading(true);
    request().then((data) => {
      setMet(data);
      setIsLoading(false);
    });
  }, []);

  const handleCrear = () => {
    history.push('/metCreate');
  };

  if (met === null) {
    return (
      <div>
        <h1>Métodos</h1>
        <Spinner animation="border" />
        <h4
          style={{
            float: 'left',
            margin: '0px 20px',
          }}
        >
          {' '}
          Cargando tabla...{'    '}
        </h4>
      </div>
    );
  }

  const columns = [
    {
      Header: 'Métodos',
      columns: [
        {
          Header: 'Identificador del Método',
          accessor: 'id_metodo',
        },
        {
          Header: 'Nombre del Método',
          accessor: 'nombre',
          Filter: SelectColumnFilter,
          width: 300,
        },
        {
          Header: 'Detalle del Método',
          accessor: 'detalle',
          Filter: SelectColumnFilter,
        },
        {
          Header: 'Descripción',
          accessor: 'descripcion',
        },
        {
          Header: 'Fecha Sistema',
          accessor: 'fsistema',
          disableFilters: true,
        },
      ],
    },
  ];
  if (isAdmin()) {
    columns.push({
      Header: 'Extras',
      columns: [
        {
          Header: 'Editar',
          width: 70,
          disableFilters: true,
          accessor: 'editar',
          Cell: (row) => (
            <div>
              <Link to={`/metUpdate?id=${row.value}`}> ✏ </Link>
            </div>
          ),
        },
      ],
    });
  }

  return (
    <div>
      <h1>Métodos</h1>
      {isAdmin() ? (
        <button
          onClick={() => handleCrear()}
          style={{
            float: 'rigth',
            display: 'inline-block',
            marginLeft: '2px',
          }}
        >
          Crear
        </button>
      ) : (
        []
      )}

      {isLoading ? (
        <>
          <p> Cargando pagina </p>
          <Spinner/>
        </>
      ) : (
        <Table data={met && met ? met : []} columns={columns} />
      )}
    </div>
  );
};

export default MetView;
