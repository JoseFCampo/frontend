import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './App';
import './static/styles/forms.scss';
import './static/styles/App.scss';
// if ('serviceWorker' in navigator) {
//   runtime.unregister();
// }

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('app'),
);
