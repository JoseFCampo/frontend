import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { AuthProvider } from './context/AuthContext';
import { FetchProvider } from './context/FetchContext';
import { createBrowserHistory} from "history";
import AppRoutes from './Routes';

const App = () =>{ 
  
  const history = createBrowserHistory({
    basename: '/argos_prod'
  });
  return(
  
  <Router basename={'argos_prod'} history={history}>
  {/* // <Router> */}
    <AuthProvider>
      <FetchProvider>
        <div className="containerApp">
          <AppRoutes />
        </div>
      </FetchProvider>
    </AuthProvider>
  </Router>
)};

export default App;
