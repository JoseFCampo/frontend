import React, { lazy, Suspense, useContext } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect, useHistory } from 'react-router-dom';
import {
 Navbar, NavDropdown, Nav, Text, Badge 
} from 'react-bootstrap';
import logo from './static/assets/svg/logo-invemar.png';
import { AuthContext } from './context/AuthContext';
import NavBar from './components/Links';

const RefView = lazy(() => import('./pages/referentes/view'));
const RefCreate = lazy(() => import('./pages/referentes/create'));
const RefUpdate = lazy(() => import('./pages/referentes/update'));

const EspViewPublic = lazy(() => import('./pages/especies/view_public'));

const EspView = lazy(() => import('./pages/especies/view'));
const EspCreate = lazy(() => import('./pages/especies/create'));
const EspUpdate = lazy(() => import('./pages/especies/update'));

const ParView = lazy(() => import('./pages/parametros/view'));
const ParCreate = lazy(() => import('./pages/parametros/create'));
const ParUpdate = lazy(() => import('./pages/parametros/update'));

const UniView = lazy(() => import('./pages/unidades/view'));
const UniCreate = lazy(() => import('./pages/unidades/create'));
const UniUpdate = lazy(() => import('./pages/unidades/update'));

const MetView = lazy(() => import('./pages/metodos/view'));
const MetCreate = lazy(() => import('./pages/metodos/create'));
const MetUpdate = lazy(() => import('./pages/metodos/update'));

const MtdView = lazy(() => import('./pages/metodologias/view'));
const MtdCreate = lazy(() => import('./pages/metodologias/create'));
const MtdUpdate = lazy(() => import('./pages/metodologias/update'));

const TemView = lazy(() => import('./pages/tematicas/view'));
const TemCreate = lazy(() => import('./pages/tematicas/create'));
const TemUpdate = lazy(() => import('./pages/tematicas/update'));

const EstCamView = lazy(() => import('./pages/estaciones/campos/view'));
const EstCamCreate = lazy(() => import('./pages/estaciones/campos/create'));
const EstCamUpdate = lazy(() => import('./pages/estaciones/campos/update'));

const EstRzView = lazy(() => import('./pages/estaciones/raiz/view'));
const EstRzCreate = lazy(() => import('./pages/estaciones/raiz/create'));
const EstRzUpdate = lazy(() => import('./pages/estaciones/raiz/update'));

const EstDetView = lazy(() => import('./pages/estaciones/detalles/view'));
const EstDetCreate = lazy(() => import('./pages/estaciones/detalles/create'));
const EstDetUpdate = lazy(() => import('./pages/estaciones/detalles/update'));

const ProView = lazy(() => import('./pages/proyectos/view'));
const ProCreate = lazy(() => import('./pages/proyectos/create'));
const ProUpdate = lazy(() => import('./pages/proyectos/update'));

const Home = lazy(() => import('./pages/home'));
const Control = lazy(() => import('./pages/control/view'));
const PageAdmin = lazy(() => import('./pages/pageAdmin'));
const PageAuth = lazy(() => import('./pages/pageAuth'));

const Projects = lazy(() => import('./pages/usuarios/Projects'));
const Login = lazy(() => import('./pages/usuarios/Login'));

const Details = lazy(() => import('./pages/usuarios/details'));

const Usuarios = lazy(() => import('./pages/usuarios/proyecto/view'));
const UsrCreate = lazy(() => import('./pages/usuarios/proyecto/create'));
const UsrUpdate = lazy(() => import('./pages/usuarios/proyecto/update'));
const UsrDetails = lazy(() => import('./pages/usuarios/proyecto/details'));
const UsrAdd = lazy(() => import('./pages/usuarios/proyecto/add'));
const PassRst = lazy(() => import('./pages/usuarios/proyecto/password'));

const Roles = lazy(() => import('./pages/usuarios/roles/view'));
const RolCreate = lazy(() => import('./pages/usuarios/roles/create'));
const RolUpdate = lazy(() => import('./pages/usuarios/roles/update'));


const Permisos = lazy(() => import('./pages/usuarios/permisos/view'));

const PermisosRol = lazy(() => import('./pages/usuarios/permisos/viewRol'));
const PermisosRolAdd = lazy(() => import('./pages/usuarios/permisos/addPerRol.js'));
const PerCreate = lazy(() => import('./pages/usuarios/permisos/create'));
const PerUpdate = lazy(() => import('./pages/usuarios/permisos/update'));

const LoadingFallback = () => <div>Cargando... </div>;

const UnauthenticatedRoutes = () => (
  <Switch>
    <Route path="/Control_Publico">
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Brand href="/">
          <img
            style={{
              width: '50px',
            }}
            alt=""
            src={logo}
            width="50"
            height="50"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Text>
          <h3
            style={{
              color: 'white',
              fontFamily: 'Open Sans Light',
              marginRight: '15px',
            }}
          >
            ARGOS
            <Badge
              variant="dark"
              style={{
                color: 'white',
                fontFamily: 'Open Sans Light',
                fontSize: '10px',
              }}
            >
              V2.0
            </Badge>
          </h3>
          {'    '}
        </Navbar.Text>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="Especies_Publico">Especies</Nav.Link>
            <Nav.Link href="Control_Publico">Control</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Control />
    </Route>

    <Route path="/login">
      <div className="auth-wrapper">
        <div className="auth-inner">
          <Login />
        </div>
      </div>
    </Route>

    <Route path="/Especies_Publico">
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Brand href="/">
          <img
            style={{
              width: '50px',
            }}
            alt=""
            src={logo}
            width="50"
            height="50"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Text>
          <h3
            style={{
              color: 'white',
              fontFamily: 'Open Sans Light',
              marginRight: '15px',
            }}
          >
            ARGOS
            <Badge
              variant="dark"
              style={{
                color: 'white',
                fontFamily: 'Open Sans Light',
                fontSize: '10px',
              }}
            >
              V2.0
            </Badge>
          </h3>
          {'    '}
        </Navbar.Text>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="Especies_Publico">Especies</Nav.Link>
            <Nav.Link href="Control_Publico">Control</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <EspViewPublic />
    </Route>
  </Switch>
);

const AuthenticatedRoute = ({ children, ...rest }) => {
  const auth = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={() =>
        auth.isAuthenticated() ? <div>{children}</div> : <Redirect to="/login" />
      }
    />
  );
};

const AdminRoute = ({ children, ...rest }) => {
  const auth = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={() =>
        auth.isAuthenticated() && auth.isAdmin() ? (
          <div>{children}</div>
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
};

const AppRoutes = () => (
  <>
    <Suspense fallback={<LoadingFallback />}>
      <Switch>
        <AdminRoute path="/admin">
          <PageAdmin />
        </AdminRoute>
        <AuthenticatedRoute path="/" exact>
          <NavBar />
          <Home />
        </AuthenticatedRoute>
        <AuthenticatedRoute path="/prj" exact>
          <NavBar />
          <Projects />
        </AuthenticatedRoute>
        <AuthenticatedRoute path="/userDetails" exact>
          <NavBar />
          <Details />
        </AuthenticatedRoute>

        <AuthenticatedRoute path="/Control">
          <NavBar />
          <Control />
        </AuthenticatedRoute>

        <AuthenticatedRoute path="/Especies">
          <NavBar />
          <EspView />
        </AuthenticatedRoute>
        <AdminRoute path="/espCreate">
          <NavBar />
          <EspCreate />
        </AdminRoute>
        <AdminRoute path="/espUpdate">
          <NavBar />
          <EspUpdate />
        </AdminRoute>

        <AdminRoute path="/Estaciones_Campos_Create">
          <NavBar />
          <EstCamCreate />
        </AdminRoute>
        <AdminRoute path="/Estaciones_Campos_Update">
          <NavBar />
          <EstCamUpdate />
        </AdminRoute>
        <AuthenticatedRoute path="/Estaciones_Campos">
          <NavBar />
          <EstCamView />
        </AuthenticatedRoute>

        <AdminRoute path="/Estaciones_Raiz_Create">
          <NavBar />
          <EstRzCreate />
        </AdminRoute>
        <AdminRoute path="/Estaciones_Raiz_Update">
          <NavBar />
          <EstRzUpdate />
        </AdminRoute>
        <AuthenticatedRoute path="/Estaciones_Raiz">
          <NavBar />
          <EstRzView />
        </AuthenticatedRoute>

        <AdminRoute path="/Estaciones_Detalles_Create">
          <NavBar />
          <EstDetCreate />
        </AdminRoute>
        <AdminRoute path="/Estaciones_Detalles_Update">
          <NavBar />
          <EstDetUpdate />
        </AdminRoute>
        <AuthenticatedRoute path="/Estaciones_Detalles">
          <NavBar />
          <EstDetView />
        </AuthenticatedRoute>

        <AuthenticatedRoute path="/Metodos">
          <NavBar />
          <MetView />
        </AuthenticatedRoute>
        <AdminRoute path="/metCreate">
          <NavBar />
          <MetCreate />
        </AdminRoute>
        <AdminRoute path="/metUpdate">
          <NavBar />
          <MetUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Metodologias">
          <NavBar />
          <MtdView />
        </AuthenticatedRoute>
        <AdminRoute path="/mtdCreate">
          <NavBar />
          <MtdCreate />
        </AdminRoute>
        <AdminRoute path="/mtdUpdate">
          <NavBar />
          <MtdUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Parametros">
          <NavBar />
          <ParView />
        </AuthenticatedRoute>
        <AdminRoute path="/parCreate">
          <NavBar />
          <ParCreate />
        </AdminRoute>
        <AdminRoute path="/parUpdate">
          <NavBar />
          <ParUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Proyectos">
          <NavBar />
          <ProView />
        </AuthenticatedRoute>
        <AdminRoute path="/proCreate">
          <NavBar />
          <ProCreate />
        </AdminRoute>
        <AdminRoute path="/proUpdate">
          <NavBar />
          <ProUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Referentes">
          <NavBar />
          <RefView />
        </AuthenticatedRoute>
        <AdminRoute path="/refCreate">
          <NavBar />
          <RefCreate />
        </AdminRoute>
        <AdminRoute path="/refUpdate">
          <NavBar />
          <RefUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Tematicas">
          <NavBar />
          <TemView />
        </AuthenticatedRoute>
        <AdminRoute path="/temCreate">
          <NavBar />
          <TemCreate />
        </AdminRoute>
        <AdminRoute path="/temUpdate">
          <NavBar />
          <TemUpdate />
        </AdminRoute>

        <AuthenticatedRoute path="/Unidades">
          <NavBar />
          <UniView />
        </AuthenticatedRoute>
        <AdminRoute path="/uniCreate">
          <NavBar />
          <UniCreate />
        </AdminRoute>
        <AdminRoute path="/uniUpdate">
          <NavBar />
          <UniUpdate />
        </AdminRoute>

        <AdminRoute path="/Permisos">
          <NavBar />
          <Permisos/>
        </AdminRoute>
        <AdminRoute path="/PermisosRol">
          <NavBar />
          <PermisosRol/>
        </AdminRoute>
        <AdminRoute path="/perAdd">
          <NavBar />
          <PermisosRolAdd/>
        </AdminRoute>
        <AdminRoute path="/perCreate">
          <NavBar />
          <PerCreate />
        </AdminRoute>
        <AdminRoute path="/perUpdate">
          <NavBar />
          <PerUpdate />
        </AdminRoute>

        <AdminRoute path="/Roles">
          <NavBar />
          <Roles />
        </AdminRoute>
        <AdminRoute path="/rolCreate">
          <NavBar />
          <RolCreate />
        </AdminRoute>
        <AdminRoute path="/rolUpdate">
          <NavBar />
          <RolUpdate />
        </AdminRoute>

        <AdminRoute path="/Usuarios">
          <NavBar />
          <Usuarios />
        </AdminRoute>
        <AdminRoute path="/usrAdd">
        <NavBar />
          <UsrAdd />
        </AdminRoute>
        <AdminRoute path="/usrCreate">
          <NavBar />
          <UsrCreate />
        </AdminRoute>
        <AdminRoute path="/usrUpdate">
          <NavBar />
          <UsrUpdate />
        </AdminRoute>
        <AdminRoute path="/usrDetails">
          <NavBar />
          <UsrDetails />
        </AdminRoute>
        <AdminRoute path="/UsrPasswordReset">
          <NavBar />
          <PassRst />
        </AdminRoute>

        <UnauthenticatedRoutes />
      </Switch>
    </Suspense>
  </>
);

AuthenticatedRoute.propTypes = {
  children: PropTypes.node,
};
AuthenticatedRoute.defaultProps = {
  children: null,
};
AdminRoute.propTypes = {
  children: PropTypes.node,
};
AdminRoute.defaultProps = {
  children: null,
};
export default AppRoutes;
