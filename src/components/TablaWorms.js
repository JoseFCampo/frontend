// Import React
import React, { useState, useEffect } from "react";

// Import React Table
import {
  useTable,
  useSortBy,
  usePagination,
} from "react-table";

import styled from "styled-components";
import "../static/styles/forms.scss";

const Styles = styled.div`
  padding: 1rem;
  display: inline-block;
  display: flex;
  white-space: pre-wrap;
  width: auto;
  heigth: 30px;
  input {
    background-color: rgb(255, 255, 255);
    border-collapse: collapse;
    border-color: rgb(118, 118, 118);
    border-style: solid;
    border-width: 1px;
    box-sizing: border-box;
    color: rgb(0, 0, 0);
    cursor: text;
    display: inline-block;
    flex-direction: column;
    font-size: 13.3333px;
    font-weight: 400;
    height: 30px;
    letter-spacing: normal;
    line-height: normal;
    margin: 0px;
    overflow-wrap: break-word;
    padding: 2px;
    resize: both;
    text-align: start;
    white-space: pre-wrap;
    width: 100px;
  }
  select {
    width: auto;
  }
  span {
    width: auto;
  }
  button {
    align-items: flex-start;
    background-color: #0d335f;
    border-color: rgb(255, 255, 255);
    border-radius: 5px;
    border-style: none;
    border-width: 0px;
    box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 2px 0px;
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: default;
    display: block;
    font-size: 13.3333px;
    font-stretch: 100%;
    font-weight: 400;
    height: 30px;
    letter-spacing: normal;
    line-height: normal;
    margin: 0px;
    padding: 1px 6px;
    text-align: center;
    width: 30.25px;
    word-spacing: 0px;
  }
`;

function MyTable({ columns, data }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      disableMultiSort: false,
    },
    useSortBy,
    usePagination
  );

  const pageSizeOptions = [10, 20, 50, 100];
  return (
    <div>
      <Styles>
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {"<<"}
        </button>{" "}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {"<"}
        </button>{" "}
        <span>
          Pagina{" "}
          <strong>
            {pageIndex + 1} de {pageOptions.length}
          </strong>{" "}
        </span>
        <span>| Ir a la pagina: </span>{" "}
        <input
          type="number"
          defaultValue={pageIndex + 1}
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          style={{ width: "100px" }}
        />{" "}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {pageSizeOptions.map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Mostrar {pageSize} Filas
            </option>
          ))}
        </select>{" "}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {">"}
        </button>{" "}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {">>"}
        </button>{" "}
      </Styles>
      <>
        <table className="table table-bordered table-hover"
          style={{
            tableHoverColor: '#85c4ffd0',
          }} {...getTableProps()}>
          <thead className="thead-light">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th scope="col" {...column.getHeaderProps()}>
                    <div>
                      <span {...column.getSortByToggleProps()}>
                        {column.render("Header")}
                        {/* Add a sort direction indicator */}
                        {column.isSorted
                          ? column.isSortedDesc
                            ? "   🔽"
                            : "   🔼"
                          : ""}
                      </span>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    </div>
  );
}

const App = (props) => {
  const data = props.data;
  const columns = [
    {
      Header: "AphiaID",
      accessor: "AphiaID",
      disableFilters: true,
    },

    {
      Header: "Nombre Cientifico",
      accessor: "scientificname",
      disableFilters: true,
    },
    {
      Header: "Autoridad",
      accessor: "authority",
      disableFilters: true,
    },
    {
      Header: "TaxonRankID",
      accessor: "taxonRankID",
      disableFilters: true,
    },
    {
      Header: "Rango",
      accessor: "rank",
      disableFilters: true,
    },
    {
      Header: "Reino",
      accessor: "kingdom",
      disableFilters: true,
    },
    {
      Header: "Phylum",
      accessor: "phylum",
      disableFilters: true,
    },
    {
      Header: "Clase",
      accessor: "class",
      disableFilters: true,
    },
    {
      Header: "Orden",
      accessor: "order",
      disableFilters: true,
    },
    {
      Header: "Familia",
      accessor: "family",
      disableFilters: true,
    },
    {
      Header: "Genero",
      accessor: "genus",
      disableFilters: true,
    },
  ];
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const updateWidth = (width) => {
    setWindowWidth(width);
  };

  useEffect(() => {
    const handleResize = () => updateWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <div>
      <MyTable columns={columns} data={data} />
    </div>
  );
};

export default App;
