import axios from "axios";
// import { URL_BACKEND } from "../../config";
const URL_BACKEND = process.env.REACT_APP_API_URL;

const Request = (method, urlApi, data, filename) => {
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: `${URL_BACKEND}/${urlApi}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
      responseType: "blob", // important
    })
      .then((res) => {
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename); //or any other extension
        document.body.appendChild(link);
        link.click();
        link.remove();
        resolve(res.statusText);
      })
      .catch((err) => {
        reject(err.response);
      });
  });
};

export default Request;
