// Import React
import React, { useState, useEffect } from 'react';
import {
  useTable,
  useFilters,
  useGlobalFilter,
  useAsyncDebounce,
  useSortBy,
  usePagination,
} from 'react-table';

import styled from 'styled-components';
import '../static/styles/forms.scss';

const Styles = styled.div`
  padding: 1rem;
  display: inline-block;
  display: flex;
  white-space: pre-wrap;
  width: auto;
  heigth: 30px;
  justify-content: center;
  input {
    background-color: rgb(255, 255, 255);
    border-collapse: collapse;
    border-color: rgb(118, 118, 118);
    border-style: solid;
    border-width: 1px;
    box-sizing: border-box;
    color: rgb(0, 0, 0);
    cursor: text;
    display: inline-block;
    flex-direction: column;
    font-size: 13.3333px;
    font-weight: 400;
    height: 30px;
    letter-spacing: normal;
    line-height: normal;
    margin: 0px;
    overflow-wrap: break-word;
    padding: 2px;
    resize: both;
    text-align: start;
    white-space: pre-wrap;
    width: 100px;
  }
  select {
    width: auto;
  }
  span {
    width: auto;
  }
  button {
    align-items: flex-start;
    background-color: #0c4465;
    border-color: rgb(255, 255, 255);
    border-radius: 5px;
    border-style: none;
    border-width: 0px;
    box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 2px 0px;
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: default;
    display: block;
    font-size: 13.3333px;
    font-stretch: 100%;
    font-weight: 400;
    height: 30px;
    letter-spacing: normal;
    line-height: normal;
    margin: 0px;
    padding: 1px 6px;
    text-align: center;
    width: 30.25px;
    word-spacing: 0px;
  }
`;

function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <Styles
      style={{
        justifyContent: 'end',
      }}
    >
      <h4>
        Buscar 🔎 :{' '}
        <input
          style={{
            width: 'auto',
          }}
          value={value || ''}
          onChange={(e) => {
            setValue(e.target.value);
            onChange(e.target.value);
          }}
          placeholder={`${count} filas...`}
        />
      </h4>
      <br />
    </Styles>
  );
}

function SelectColumnFilter({column: { filterValue, setFilter, preFilteredRows, id },}) {
  const options = React.useMemo(() => {
    const options = new Set();
    preFilteredRows.forEach((row) => {
      options.add(row.values[id]);
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  // Render a multi-select box
  return (
    <select
      style={{
        maxWidth: '180px',
      }}
      value={filterValue}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
    >
      <option value="">All</option>
      {options.sort().map((option, i) => (
        <option key={i} value={option}>
          {option}
        </option>
      ))}
    </select>
  );
}

function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length;
  return (
    <input
      type="Text"
      value={filterValue || ''}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`${count} Datos `}
      style={{
        margin: '0px',
        width: '100px',
      }}
    />
  );
}

function MyTable({ columns, data, noPagination }) {
  const defaultColumn = React.useMemo(() => {
    return {
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
    };
  }, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, filters },
    state,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      disableMultiSort: false,
      initialState: {
        pageSize: 50,
      },
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination,
  );
  const pageSizeOptions = [10, 20, 50, 100];
  // Render the UI for your table
  return (
    <div>
      <GlobalFilter
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={state.globalFilter}
        setGlobalFilter={setGlobalFilter}
      />
      <div
        className="table-responsive"
        style={{
          maxWidth: screen.width - 20,
        }}
      >
        <table
          className="table table-bordered table-hover"
          style={{
            tableHoverColor: '#85c4ffd0',
          }}
          {...getTableProps()}
        >
          <thead className="thead-light">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th scope="col" {...column.getHeaderProps()}>
                    <div>
                      <span {...column.getSortByToggleProps()}>
                        {column.render('Header')}
                        {/* Add a sort direction indicator */}
                        {column.isSorted
                          ? column.isSortedDesc
                            ? '     🔽'
                            : '     🔼'
                          : '     '}
                      </span>
                    </div>
                    {/* Render the columns filter UI */}
                    <div
                      style={{
                        margin: 'auto',
                        marginBottom: '0',
                      }}
                    >
                      {column.canFilter ? column.render('Filter') : null}
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <Styles
        style={{
          display: noPagination,
        }}
      >
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <span>
          Pagina{' '}
          <strong>
            {pageIndex + 1} de {pageOptions.length}
          </strong>{' '}
        </span>
        <span>| Ir a la pagina: </span>{' '}
        <input
          type="number"
          defaultValue={pageIndex + 1}
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          style={{
            width: '100px',
          }}
        />{' '}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {pageSizeOptions.map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Mostrar {pageSize} Filas
            </option>
          ))}
        </select>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
      </Styles>
    </div>
  );
}

const App = (props) => {
  const { data } = props;
  const { columns } = props;
  const nopag = props.nopag ? 'none' : 'flex';
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const updateWidth = (width) => {
    setWindowWidth(width);
  };

  useEffect(() => {
    const handleResize = () => updateWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
    setPageSize(50);
  }, []);
  return (
    <div>
      {/* <CssBaseline /> */}
      <MyTable columns={columns} data={data} noPagination={nopag} />
    </div>
  );
};

export default App;
export { SelectColumnFilter, Styles };
