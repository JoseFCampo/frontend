import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg viewBox="0 0 73.5 85" {...props}>
      <switch>
        <g>
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill="#2B478E"
            d="M12.5 81.4h-5V67.7h5V72H16c.8 0 1.3.2 1.9.7.2-.5.5-.7.9-.7h8.7c.4 0 .6.4.8.6.5-.3 1.1-.6 1.6-.6H46c1 0 2.1 1.1 2.6 1.7V72h8.3c.7 0 2 .9 2.7 1.9.4-.7 1.6-1.9 2.3-1.9h5v3.2c-1.3.1-1.8.5-2.3 1.4v4.8h-13c-1.1-.2-1.9-.9-2.6-1.8v1.8H12.5z"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill="#FFF"
            d="M60.2 80.5v-5.6c.2-1 .9-1.5 1.7-1.9H66v1.4c-1.3.4-1.9 1.1-2.3 2v4.1h-3.5zm-4.7 0H59v-5.1c0-1.4-1.1-2.2-2.1-2.4h-7.4v1.9h5.1v1.3h-5.1v1.5c.1 1.8.9 2.6 2.3 2.8H54v-3.1c0-.9 1.4-.9 1.4 0v3.1h.1zm-21.7 0V73h12.4c.8.2 1.4.9 2 1.8v5.7h-3.5v-5.1h-1.4v5.1h-4v-5.1h-1.4v5.1h-4.1zm-4.5-3.1c0 .4.3.6.7.6.4 0 .7-.3.7-.6v-2.1c0-.4-.3-.6-.7-.6-.4 0-.7.3-.7.6v2.1zm-4 3.1l3.3-6.8c.2-.3.9-.6 1.3-.7h2.7v5.3h-3.9v1.3h3.9v.9h-7.3zm-6.7-7.3l3.9 7.2H24l3.5-7.2-.2-.2h-3.7v5.4h-1.2V73h-3.5l-.3.2zM16 80.5h-3.1V73h2.9c.7 0 1.4.1 1.9 1l3.6 6.5h-3.9v-5.1H16v5.1zM8.4 73h3.2v7.5H8.4V73zm0-4.3h3.2v2.8H8.4v-2.8z"
          />
          <radialGradient
            id="prefix__a"
            cx={36.834}
            cy={35.038}
            r={33.147}
            gradientUnits="userSpaceOnUse"
          >
            <stop offset={0} stopColor="#68a7dd" />
            <stop offset={0.16} stopColor="#5d96cf" />
            <stop offset={0.727} stopColor="#395da0" />
            <stop offset={1} stopColor="#2b478e" />
          </radialGradient>
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill="url(#prefix__a)"
            stroke="#FFF"
            strokeWidth={0.5}
            strokeMiterlimit={10}
            d="M53.9 67.3s1.1-3.8 6.2-4.6l10.7-32.5s-3-2.5-2.5-7.2L40.5 2.8s-3.4 2.5-7.4 0L5.3 22.9s1 4.4-2.5 7.3l10.5 32.4s4.7.6 6.2 4.7h34.4z"
          />
          <g
            clipRule="evenodd"
            fill="none"
            stroke="#FFF"
            strokeWidth={0.5}
            strokeMiterlimit={10}
          >
            <path d="M63.2 28.7s1.1 6.4-5.6 13L65 49" />
            <path d="M53 59.6s4-3.8 2.9-13.1l10.7-2.8M53 59.6s-5.7 2.6-14.1-1.6l-4.6 9.5" />
            <path d="M20.5 59.6s5 2.6 13.4-1.6l6.4 9.5M36.8 9.7s6.2 1.1 10.6 9.4l9.2-4.9" />
            <path d="M63.2 28.7s-2.5-5-11.7-6.6l.5-11.3M10.6 28.5s3-5.5 12.3-7l-1.6-10.3" />
            <path d="M37.2 9.7s-5.5.7-10.1 8.9l-10.5-4.1" />
            <g>
              <path d="M20.5 59.6s-4.4-4.6-3-13.8L7.2 44.3" />
              <path d="M10.5 28.6s-1 5.5 5.5 12.3l-7.1 8.8" />
            </g>
          </g>
          <g fill="none" stroke="#FFF" strokeWidth={0.5} strokeMiterlimit={10}>
            <path
              clipRule="evenodd"
              d="M36.8 34.9c-2.8 0-2.2-3.8-3.7-5.6-1.3-1.6-3.1-2-3.5-3.6 3.3.7 4.2-1.3 3.9-6 0-2.7 2-7.8 3.3-10-1.8-3.6-2.8-4-3.7-6.9 1.1.7 2.4 1.1 3.7 1.1 1.3 0 2.6-.4 3.7-1.1-.9 2.9-1.8 3.3-3.7 6.9 1.3 2.2 3.3 7.4 3.3 10-.2 4.7.7 6.7 3.9 6-.2 1.6-2.2 2-3.5 3.6-1.5 1.8-.9 5.6-3.7 5.6h0z"
            />
            <path d="M36.8 29.2v-5.3" />
            <path
              clipRule="evenodd"
              d="M34.5 36.5c-.9 2.7-4.2 1.1-6.5 1.8-1.8.9-2.9 2.4-4.5 2.4 1.6-2.9 0-4.4-4.7-5.8-2.2-.7-6.5-4.2-8.2-6.2-4 .7-4.9 1.6-7.8 1.6 1.1-.9 1.9-2.2 2.2-3.3.4-1.3.4-2.7.2-4 2.2 1.8 2.5 2.9 5.3 5.8 2.5-.4 8-.9 10.5 0 4.5 1.6 6.5 1.3 6.9-2 1.6.7 1.3 2.9 2.5 4.4 1.4 2 5 2.4 4.1 5.3h0z"
            />
            <path d="M28.1 34.4l-5.2-1.3" />
            <path
              clipRule="evenodd"
              d="M39 36.3c-.9-2.6 2.9-3.3 4-5 1.1-1.7 1.1-3.7 2.5-4.6.4 3.3 2.5 3.5 7 2 2.5-.9 8.1-.4 10.8 0 2.7-2.8 2.9-3.9 5.2-5.7-.2 1.5 0 2.6.4 3.9.4 1.3.9 2.4 2 3.3-2.9 0-3.6-.9-7.6-1.5-2 2-6.3 5.5-8.5 6.1-4.7 1.3-6.3 2.8-4.5 5.7-1.8.2-2.9-1.5-4.7-2.2-2.4-.9-5.7.9-6.6-2h0z"
            />
            <path d="M45.1 34.1l5.1-1.3" />
            <path
              clipRule="evenodd"
              d="M35.4 39.2c2.4 1.8-.4 4.4-.2 6.6.2 2 1.3 3.5.7 5.1-2.2-2.4-4-1.3-6.7 2.4-1.3 2.2-6.2 5.1-8.7 6.2-.4 4 0 4.9-.9 7.8-.4-1.3-1.3-2.3-2.4-3.1-1.1-.7-2.2-1.3-3.8-1.6 2.4-1.5 3.6-1.3 7.1-3.1.4-2.7 1.8-8 3.3-10 2.9-3.8 3.3-6 .2-7.3 1.1-1.1 3.1-.2 5.1-.9 2.3-.3 3.8-3.9 6.3-2.1h0z"
            />
            <path d="M31.8 44l-3.4 4" />
            <g>
              <path
                clipRule="evenodd"
                d="M38.1 39.2c2.5-1.8 4 1.5 6.2 2.2 2 .4 4-.2 5.1.9-2.9 1.3-2.7 3.5.4 7.3 1.3 2 2.7 7.3 3.1 9.9 3.6 1.8 4.7 1.5 7.1 3.1-1.6.2-2.7.8-3.8 1.5-1.2.8-2 1.8-2.5 3.1-.9-2.9-.4-3.8-.9-7.7-2.5-1.3-7.1-4-8.7-6.2-2.7-3.8-4.5-4.9-6.7-2.4-.7-1.5.7-3.1.7-5.1.4-2.2-2.2-4.8 0-6.6h0z"
              />
              <path d="M41.3 43.7l3.5 4" />
            </g>
          </g>
        </g>
      </switch>
    </svg>
  );
}

export default SvgComponent;
