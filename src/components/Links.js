import React, { useContext } from 'react';
import '../static/styles/navbar.scss';
import { Navbar, NavDropdown, Nav, Badge } from 'react-bootstrap';
import { AuthContext } from '../context/AuthContext';

import logo from '../static/assets/svg/logo-invemar.png';

const TopNav = () => {
  const { logout, project, isAuthenticated, isAdmin } = useContext(AuthContext);

  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        variant="dark"
        // style={{
        //   height: '55px',
        // }}
      >
        <Navbar.Brand href="">
          <img
            style={{
              width: '45px',
            }}
            alt=""
            src={logo}
            width="45"
            height="45"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Text href="">
          <h3
            
            style={{
              color: 'white',
              // fontFamily: 'Open Sans Light',
              marginRight: '15px',
            }}
          >
            ARGOS
            <Badge
              variant="dark"
              style={{
                color: 'white',
                // fontFamily: 'Open Sans Light',
                fontSize: '10px',
              }}
            >
              V2.0
            </Badge>
          </h3>
          {'    '}
        </Navbar.Text>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            {isAuthenticated() ? (
              <>
                <Nav.Link href="">Inicio</Nav.Link>
                <Nav.Link href="Control">Control</Nav.Link>
                <Nav.Link href="Especies">Especies</Nav.Link>
                <Nav.Link href="Metodos">Métodos</Nav.Link>
                <Nav.Link href="Metodologias">Metodologías</Nav.Link>
                <Nav.Link href="Parametros">Parámetros</Nav.Link>
                <Nav.Link href="Proyectos">Proyectos</Nav.Link>
                <Nav.Link href="Referentes">Referentes</Nav.Link>
                <Nav.Link href="Tematicas">Temáticas</Nav.Link>
                <Nav.Link href="Unidades">Unidades</Nav.Link>
                <NavDropdown title="Estaciones" id="basic-nav-dropdown">
                  <NavDropdown.Item href="Estaciones_Raiz">
                    Informacion Base
                  </NavDropdown.Item>
                  <NavDropdown.Item href="Estaciones_Campos">
                    Campos Adicionales
                  </NavDropdown.Item>
                </NavDropdown>
                {project && isAdmin() ? (
                  <NavDropdown title="Usuarios" id="basic-nav-dropdown">
                    <NavDropdown.Item href="Usuarios">Usuarios</NavDropdown.Item>
                    <NavDropdown.Item href="Roles">Roles</NavDropdown.Item>
                    <NavDropdown.Item href="Permisos">Permisos</NavDropdown.Item>
                  </NavDropdown>
                ) : (
                  []
                )}
              </>
            ) : (
              <>
                <Nav.Link href="Especies">Especies</Nav.Link>
                <Nav.Link href="Control">Control</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>

        <Navbar.Collapse
          id="perfil"
          // className="justify-content-end"
          style={{
            marginLeft: '0px',
            width: '5%',
          }}
        >
          <NavDropdown
            title="Mi Perfil"
            id="collasible-nav-dropdown"
            className="dropdown-perfil"
          >
            <NavDropdown.Item href="userDetails">Datos Personales</NavDropdown.Item>
            <NavDropdown.Item href="prj">Cambiar Proyecto</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item onClick={logout}>Cerrar Sesion</NavDropdown.Item>
          </NavDropdown>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default TopNav;
