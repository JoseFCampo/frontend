import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

const Styles = styled.div`
    .ok {
        margin: 5px 2px;
        padding: 8px;
        border: 1px solid;
        height: 40px;
        border-radius: 5px;
        justify-content: center;
        color: #099409;
        background-color: #c2ffc2;
        border-color: #01e201;
    }
    .error {
        margin: 5px 2px;
        padding: 8px;
        border: 1px solid;
        height: 40px;
        border-radius: 5px;
        justify-content: center;
        color: #a90808;
        background-color: #ffc2c2;
        border-color: #e20101;
    }
`;

const Message = ({ response }) => {
    const history = useHistory();
    if ((response.status == 200) | (response.status == 201)) {
        console.log(response.data.message)
        return (
            <Styles>
                <div className="ok"> 🟢 {response.data.message}</div>
            </Styles>
        );
    }
    return (
        <Styles>
            <div className="error">🛑{response.message}</div>;
        </Styles>
    );
};

export default Message;
